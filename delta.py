#!/usr/bin/env python3

from sympy import *

def trysubs(obj, subsdict):
	if isinstance(obj, int):
		return obj
	else:
		return obj.subs(subsdict)
class Func():
	def __init__(self):
		self.delta = []
		self.higher = []
		self.lower = []
	def whole(self):
		ret = 0
		for v in self.delta:
			if v[2] == 0:
				ret += v[1]*DiracDelta(z-v[0])
			else:
				ret += v[1]*DiracDelta(z-v[0],v[2])
		for v in self.higher:
			ret += v[1]*Heaviside(z-v[0])
		for v in self.lower:
			ret += v[1]*Heaviside(-z+v[0])
		return ret
	def whole_just_delta(self):
		ret = 0
		for v in self.delta:
			if v[2] == 0:
				ret += v[1]*DiracDelta(z-v[0])
			else:
				ret += v[1]*DiracDelta(z-v[0],v[2])
		return ret
	def whole_without_delta(self):
		ret = 0
		for v in self.higher:
			ret += v[1]*Heaviside(z-v[0])
		for v in self.lower:
			ret += v[1]*Heaviside(-z+v[0])
		return ret
	#def region(self, zlow, zhigh):
	#	for v in self.delta:
	#		if zlow <= v[0] <= zhigh:
	#			raise ValueError("This region has delta functions")		
	def add_onto(self,G):
		for v in G.delta:
			self.delta.append((v[0],v[1],v[2]))
		for v in G.lower:
			self.lower.append((v[0],v[1]))
		for v in G.higher:
			self.higher.append((v[0],v[1]))
	def shift(self, shift):
		for i in range(0, len(self.delta)):
			temp = (self.delta[i][0]-shift, self.delta[i][1], self.delta[i][2])
			self.delta[i] = temp
		for i in range(0, len(self.higher)):
			temp = (self.higher[i][0]-shift, self.higher[i][1].subs(z, z+shift))
			self.higher[i] = temp
		for i in range(0, len(self.lower)):
			temp = (self.lower[i][0]-shift, self.lower[i][1].subs(z, z+shift))
			self.lower[i] = temp
		return self
	def subs(self, subsdict):
		if z in subsdict or "z" in subsdict:
			raise ValueError("dont substitute z")
		for i in range(0, len(self.delta)):
			temp = (trysubs(self.delta[i][0], subsdict), trysubs(self.delta[i][1], subsdict), self.delta[i][2])
			self.delta[i] = temp
		for i in range(0, len(self.higher)):
			temp = (trysubs(self.higher[i][0], subsdict), trysubs(self.higher[i][1], subsdict))
			self.higher[i] = temp
		for i in range(0, len(self.lower)):
			temp = (trysubs(self.lower[i][0], subsdict), trysubs(self.lower[i][1], subsdict))
			self.lower[i] = temp
		return self
	def flip(self):
		for i in range(0, len(self.delta)):
			temp = (-self.delta[i][0], (-1)**self.delta[i][2]*self.delta[i][1], self.delta[i][2])
			self.delta[i] = temp
			
		temphigher = self.higher
		templower = self.lower
		self.lower =  []
		self.higher = []
		for v in temphigher:
			self.lower.append((-v[0],v[1].subs(z,-z)))
		for v in templower:
			self.higher.append((-v[0],v[1].subs(z,-z)))
		return self
	def clone(self):
		ret = Func()
		for v in self.delta:
			ret.delta.append((v[0],v[1], v[2]))
		for v in self.lower:
			ret.lower.append((v[0],v[1]))
		for v in self.higher:
			ret.higher.append((v[0],v[1]))
		return ret
	#def __add__
	#def __mult__
	
def mult_clone(F,fac):
	ret = Func()
	for v in F.delta:
		ret.delta.append((v[0],fac*v[1], v[2]))
	for v in F.lower:
		ret.lower.append((v[0],fac*v[1]))
	for v in F.higher:
		ret.higher.append((v[0],fac*v[1]))
	return ret

def diff_clone(F):
	ret = Func()
	for v in F.delta:
		ret.delta.append((v[0], v[1], v[2]+1))
	for v in F.higher:
		ret.higher.append((v[0],diff(v[1],z)))
		ret.delta.append((v[0], v[1].subs(z, v[0]), 0))
	for v in F.lower:
		ret.lower.append((v[0],diff(v[1],z)))
		ret.delta.append((v[0], -v[1].subs(z, v[0]), 0))
	return ret


V0 = symbols("V0")
c = symbols("c")
F = symbols("F")
xb = symbols("xb")
z = symbols("z")
x = symbols("x")
t = symbols("t")
xs = symbols("xs")
r = symbols("r")

def do(V):
	return tanh(V/2)
def ds(V):
	return exp(-V)*(1+tanh(V/2)) # = e^-V*( 1+e^(-V) +  (1-e^-V)  )/(1+e^-V) = e^-V*( 2  )/(1+e^-V) = 2/(1+e^V)
	#return 2/(exp(V)+1)
def mo(V,F):
	return 2*F*tanh(V/2)*(1+tanh(V/2))
	return exp(V)*2*F*tanh(V/2)*2/(exp(V)+1)
def ms(V,F):
	return exp(-V)*2*F*tanh(V/2)*(1+tanh(V/2))
	return 2*F*tanh(V/2)*2/(exp(V)+1)
def k(V,F):
	return 2*F*tanh(V/2)

V = symbols("V")

def prove_step(g,h):
	jump = g.whole() + g.whole().subs(z,-z) - exp(V)*h.whole() - exp(V)*h.whole().subs(z,-z)
	#print("jump: 0 =", simplify(jump))
	print("jump: 0 =", latex(simplify(jump)))

	current = Func()
	current.add_onto(mult_clone(g, 2*F))
	current.add_onto(mult_clone(h, -2*F))
	current.add_onto(mult_clone(g, 2*F).flip())
	current.add_onto(mult_clone(h, -2*F).flip())

	current.add_onto(mult_clone(diff_clone(g),-1))
	current.add_onto(mult_clone(diff_clone(g),-1).flip())
	current.add_onto(diff_clone(h))
	current.add_onto(diff_clone(h).flip())

	curw = current.whole().subs(tanh(V/2), (c-1)/(c+1)).subs(exp(-V),1/c)
	assert(not V in curw.free_symbols)
	#print("current: 0 =", simplify(curw))
	print("current: 0 =", latex(simplify(curw)))

def prove_step_whole(gw,hw):
	#gw = g.whole()
	#hw = h.whole()
	jump = gw + gw.subs(z,-z) - exp(V)*hw - exp(V)*hw.subs(z,-z)
	#print("jump: 0 =", simplify(jump))
	print("jump: 0 =", latex(simplify(jump)))

	#current = Func()
	#current.add_onto(mult_clone(diff_clone(g),-1))
	#current.add_onto(mult_clone(diff_clone(g),-1).flip())
	#current.add_onto(diff_clone(h))
	#current.add_onto(diff_clone(h).flip())

	#truesubs = {exp(-2*F*(-xb + z)*tanh(V/2))*tanh(V/2)*DiracDelta(-xb + z): tanh(V/2)*DiracDelta(-xb + z)}
	
	curw = 2*F*gw - 2*F*hw + 2*F*gw.subs(z,-z) - 2*F*hw.subs(z,-z) + diff(hw,z) + diff(hw,z).subs(z,-z) - diff(gw,z) - diff(gw,z).subs(z,-z)
	curw = curw.subs(tanh(V/2), (c-1)/(c+1)).subs(exp(-V),1/c)
	assert(not V in curw.free_symbols)
	#print("current: 0 =", simplify(curw))
	print("intcurrent: 0 =", integrate(simplify(curw), z))
	print("current: 0 =", latex(simplify(curw)))

def get5_1():
	h = Func()
	h.higher.append((xb, ms(V,-F)*exp(k(V,-F)*(-xb+z))))
	h.delta.append((xb, ds(V),0))

	g = Func()
	g.lower.append((-xb, mo(V,-F)*exp(k(V,-F)*(-xb-z))))

	g.delta.append((-xb,do(V),0))
	g.delta.append((xb,1,0))

	return g,h

def prove5_1():
	g,h = get5_1()
	prove_step(g,h)

	g2 = h.clone().subs({V:-V})
	h2 = g.clone().subs({V: -V})
	prove_step(g2,h2)

	g3 = g.clone().flip().subs({F:-F})
	h3 = h.clone().flip().subs({F:-F})
	prove_step(g3,h3)

	g4 = h.clone().flip().subs({F:-F}).subs({V:-V})
	h4 = g.clone().flip().subs({F:-F}).subs({V:-V})
	prove_step(g4,h4)

def get5_2():
	h = Func()
	h.delta.append((xb, 1,0))

	g = Func()
	g.delta.append((xb,(1+exp(V))/2,0))
	g.higher.append((xb, F*(exp(V)-1)))

	g.delta.append((-xb, exp(V),0))

	g.delta.append((-xb,-(1+exp(V))/2,0))
	g.lower.append((-xb, -F*(exp(V)-1)))
	
	return g,h
	
def prove5_2():
	g,h = get5_2()
	prove_step(g,h)

	g2 = h.clone().subs({V:-V})
	h2 = g.clone().subs({V: -V})
	prove_step(g2,h2)

	g3 = g.clone().flip().subs({F:-F})
	h3 = h.clone().flip().subs({F:-F})
	prove_step(g3,h3)

	g4 = h.clone().flip().subs({F:-F}).subs({V:-V})
	h4 = g.clone().flip().subs({F:-F}).subs({V:-V})
	prove_step(g4,h4)

def get5_3():
	h = Func()
	h.delta.append((0,1,0))

	g = Func()
	g.delta.append((0,exp(V),0))
	g.higher.append((0, F*(exp(V)-1)))
	g.lower.append((0, -F*(exp(V)-1)))

	return g,h
	
def prove5_3():
	g,h = get5_3()
	prove_step(g,h)

	#g2 = h.clone().subs({V:-V})
	#h2 = g.clone().subs({V: -V})
	#prove_step(g2,h2)

	#g3 = g.clone().flip().subs({F:-F})
	#h3 = h.clone().flip().subs({F:-F})
	#prove_step(g3,h3)

	#g4 = h.clone().flip().subs({F:-F}).subs({V:-V})
	#h4 = g.clone().flip().subs({F:-F}).subs({V:-V})
	#prove_step(g4,h4)

def myrefine(expr):
	return expr.subs({Heaviside(-xp): 0,
			  DiracDelta(-xp + z)*Heaviside(-z): 0
	})

def calc_from_higher_g_lower_h(gor,hor):
	do = Function("do")
	mo = Function("mo")
	ds = Function("ds")
	ms = Function("ms")
	k = Function("k")
	
	gor = trysubs(gor, {z: xb})
	hor = trysubs(hor, {z: xb})
	retg = myrefine(integrate(gor*(
		DiracDelta(z-xb)
		+do(V)*DiracDelta(z+xb)
		+mo(V,-F)*exp(k(V,-F)*(-xb-z))*Heaviside(-z-xb)
	), (xb,0,oo)))
	reth = myrefine(integrate(gor*(
		ds(V)*DiracDelta(z-xb)
		+ms(V,-F)*exp(k(V,-F)*(-xb+z))*Heaviside(z-xb)
	), (xb,0,oo)))

	print(retg)
	print(reth)
	prove_step_whole(retg, reth)
	
	return
	



#prove_step_whole
class VecMP:
	def __init__(self):
		self.g_delt = 0
		self.h_delt = 0
		self.g_exp_pos = 0
		#self.h_exp_neg = 0
	def whole(self):
		return DiracDelta(z+xb)*self.g_delt *self.g_exp_pos* Heaviside(-z-xb)*exp(k(V,F)*z) , DiracDelta(z-xb)*self.h_delt
class VecPM:
	def __init__(self):
		self.g_delt = 0
		self.h_delt = 0
		self.g_exp_pos = 0
		self.g_exp_neg = 0
		self.h_exp_pos = 0
		self.h_exp_neg = 0
	def whole(self):
		return DiracDelta(z-xb)*self.g_delt + self.g_exp_pos*Heaviside( z-xb)*exp( k(V,F)*z) + self.g_exp_neg*Heaviside( z-xb)*exp( -k(V,F)*z)\
		    ,  DiracDelta(z+xb)*self.h_delt + self.h_exp_neg*Heaviside(-z-xb)*exp(-k(V,F)*z) + self.h_exp_pos*Heaviside(-z-xb)*exp(  k(V,F)*z)
def Smatrix(vec):
	ret = VecPM()
	
	ret.g_delt +=    vec.g_delt * do(V)
	ret.h_delt +=    vec.g_delt * ds(V)
	ret.g_exp_pos += vec.g_delt * mo(V,F)*exp(-xb*k(V,F))
	ret.h_exp_neg += vec.g_delt * ms(V,F)*exp(-xb*k(V,F))

	ret.g_exp_pos += vec.g_exp_pos * mo(V,-F)/(2*k(V,-F))*exp(2*k(V,-F)*xb)
	ret.g_exp_neg += vec.g_exp_pos * (do(V) - mo(V,-F)/(2*k(V,-F)))
	ret.h_exp_pos += vec.g_exp_pos * exp(-V)*( 1 + do(V) - mo(V,F)/(2*k(V,F))  )
	ret.h_exp_neg += vec.g_exp_pos * exp(-V)*( mo(V,F)/(2*k(V,F))*exp(2*k(V,F)*xb)  )
	
	return ret
	
bas = VecMP()
bas.g_exp_pos = 1
g1,h1 = bas.whole()
g2,h2 = Smatrix(bas).whole()
#print(g1+g2)
#print(DiracDelta(z+xb) + do(V)*DiracDelta(z-xb) + mo(V,F)*Heaviside(z-xb)*exp(k(V,F)*(-xb+z)))
prove_step_whole(g1+g2, h1+h2)



exit(0)
# -r < xa < xs   and -r < xs < r
bas = []
bas.append(Q.is_true(-r < xa))
bas.append(Q.is_true(xa < xs))
bas.append(Q.is_true(-r < xs))
bas.append(Q.is_true(xs < r))
g = Piecewise( (DiracDelta(z-xa)*exp(-2*F*xa), (-r <= z) & (z <= xs)))
h = Piecewise( (0, (xs <= z) & (z <= r)))
	

exit(0)



# curn = current.whole_without_delta()
# curn = curn.subs(t, 20000).subs(F,0.02).subs(a, 50).subs(V,1).subs(c, exp(1))
# for z_s in range(0,200,4):
# 	if z_s != 50:
# 		cur = curn.subs(z, z_s).evalf()
# 		if cur > 1e-100:
# 			print("bad current")

# curd = current.whole_just_delta()
# print(latex(simplify(curd)))

xa = symbols("xa")

# g_mr_r =  exp(-2*F*xa)*DiracDelta(z-xa) \
# 	+ exp(-2*F*xa)*do(V0)*DiracDelta(z+xa-2*xs) \
# 	+ exp(-2*F*xa)*mo(V0,F)*exp( (z+xa-2*xs)*k(V0,F) )*Heaviside(z+xa-2*xs)

# h_mrp2xs_r = exp(-2*F*xa)*ds(V0)*DiracDelta(z-xa) \
# 	+ exp(-2*F*xa)*ms(V0,F)*exp( (-z+xa)*k(V0,F) )*Heaviside(-z+xa)

#g, h = symbols('g, h', cls=Function)
#eq = (Eq(Derivative(x(t),t), 12*t*x(t) + 8*y(t)), Eq(Derivative(y(t),t), 21*x(t) + 7*t*y(t)))
#eq = (Eq)
#print(dsolve(eq))

g_mr_xs = exp(-2*F*xa)*DiracDelta(z-xa)
h_xs_r = 0

tau = symbols("tau")
#newg = exp(2*F*tanh(V0/2)*z)* g_mr_xs.subs(z,0) + exp(2*F*tanh(V0/2)*z) * integrate(exp(-2*F*tanh(V0/2)*tau)*( \
#2/(1+exp(-V0))*diff(h_xs_r, z).subs(z,tau) - tanh(V0/2)*diff(g_mr_xs,z).subs(z,-tau) + 2*F*tanh(V0/2)*g_mr_xs.subs(z,-tau) \
#) , (tau,0,z))
newg = exp(2*F*tanh(V0/2)*z)* g_mr_xs.subs(z,0) + exp(2*F*tanh(V0/2)*z) * integrate((sinh(-2*F*tanh(V0/2)*tau)+cosh(-2*F*tanh(V0/2)*tau))*( \
2/(1+exp(-V0))*diff(h_xs_r, z).subs(z,tau) - tanh(V0/2)*diff(g_mr_xs,z).subs(z,-tau) + 2*F*tanh(V0/2)*g_mr_xs.subs(z,-tau) \
) , (tau,0,z))
newg = integrate((sinh(-2*F*tanh(V0/2)*tau)+cosh(-2*F*tanh(V0/2)*tau))*( \
 - diff(g_mr_xs,z).subs(z,-tau) \
) , (tau,0,z))

newg = integrate((sinh(tau)+cosh(tau))*( \
 - DiracDelta(tau,1) \
) , (tau,-1,z))

#newg = refine(newg, Q.negative(xa))
print(newg)


print(integrate((sinh(x)+cosh(x))* DiracDelta(x,1) , (x,-1,z)))

print(integrate((sinh(x-1)+cosh(x-1))* DiracDelta(x-1,1) , (x,0,z)))
print(integrate((sinh(x)+cosh(x))* DiracDelta(x-1,1) , (x,0,z)))



exit(0)

fl = 2*F*tanh(V/2)*(1+tanh(V/2))*exp(2*F*a*(1-tanh(V/2)))*exp(2*F*z*tanh(V/2))*Heaviside(z-a)

fr = exp(-V)*2*F*tanh(V/2)*(1+tanh(V/2))*exp(2*F*a*(1-tanh(V/2)))*exp(-2*F*z*tanh(V/2))*Heaviside(-z-a)

jump = fl + fl.subs(z,-z) - exp(V)*fr - exp(V)*fr.subs(z,-z)
print("jump: 0 =", simplify(jump))

current = 2*F*fl + 2*F*fl.subs(z,-z) - diff(fl,z) - diff(fl,z).subs(z,-z) \
	 -2*F*fr - 2*F*fr.subs(z,-z) + diff(fr,z) + diff(fr,z).subs(z,-z)
#print("current: 0 =", latex(simplify(current)))

curn = current.subs(t, 20000).subs(F,0.02).subs(a, 50).subs(V,1)
for z_s in range(0,200,4):
	if z_s != 50:
		cur = curn.subs(z, z_s).evalf()
		if cur > 1e-20:
			print("bad current")

exit(0)


fl = exp(2*F*a)*DiracDelta(z+a) \
	+ tanh(V/2)*exp(2*F*a)*DiracDelta(z-a) \
	+ 2*F*tanh(V/2)*(1+tanh(V/2))*exp(2*F*a*(1-tanh(V/2)))*exp(2*F*z*tanh(V/2))*Heaviside(z-a)

fr = + exp(-V)*(1+tanh(V/2))*exp(2*F*a)*DiracDelta(z+a) \
	+ exp(-V)*2*F*tanh(V/2)*(1+tanh(V/2))*exp(2*F*a*(1-tanh(V/2)))*exp(-2*F*z*tanh(V/2))*Heaviside(-z-a)

jump = fl + fl.subs(z,-z) - exp(V)*fr - exp(V)*fr.subs(z,-z)
#print("jump: 0 ", simplify(jump))

current = 2*F*fl + 2*F*fl.subs(z,-z) - diff(fl,z) - diff(fl,z).subs(z,-z) \
	 -2*F*fr - 2*F*fr.subs(z,-z) + diff(fr,z) + diff(fr,z).subs(z,-z)

integrand = exp(-(x-z)**2/t + 2*F*x - F*F*t) *fl/sqrt(pi*t)
	

#print(latex(simplify(current)))
