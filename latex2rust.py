#!/usr/bin/env python3

from latex2sympy import *

def makeFloat(rust):
	i = 0
	while i < len(rust):
		if rust[i].isdigit() and (i==0 or rust[i-1] in ["(", "-", " "]) and rust[i+1] in [")", "*", " "]:
			rust = rust[:i+1] + "." + rust[i+1:]
		i += 1
	return rust
def tex2rust(tex):
	print(makeFloat(sympy.printing.rust.rust_code(latex2sympy(tex.replace("\\lou", "(").replace("\\rou", ")")
								  , ConfigL2S())
					    .subs(symbols("\\beta"), symbols("beta"))
					    .subs(symbols("e"),symbols("euler"))
	)
	      .replace("euler","std::f64::consts::E")
	))

tex2rust(r"\frac{2e^{-2F_1 x_a}}{1+e^{\beta V_0}}")
tex2rust(r"2F_1\frac{1-e^{\beta V_0}}{1+e^{\beta V_0}}\frac{2e^{-2F_1 x_a}}{1+e^{\beta V_0}} \exp\lou 2F_1\frac{1-e^{\beta V_0}}{1+e^{\beta V_0}}(z-x_a)\rou")
print("--------------")
tex2rust(r"1/(\tanh\frac{\beta V_0}2) e^{-2F_1x_a}")
tex2rust(r"2F_1 e^{-2F_1x_a}\lou 1+1/(\tanh\frac{\beta V_0}2)\rou\exp\lou 2F(z-2x_s+x_a)\rou")