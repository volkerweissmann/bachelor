import sys
import os
import numpy as np
import matplotlib.pyplot as plt

pos = 0
def press(event):
	global pos
	sys.stdout.flush()
	oldpos = pos
	if event.key == "right" and os.path.exists("out/"+str(pos+1)+".out"):
		pos += 1
	elif event.key == "left" and os.path.exists("out/"+str(pos-1)+".out"):
		pos -= 1
	dat = np.loadtxt("out/"+str(pos)+".out")
	line.set_xdata(np.arange(0,len(dat)))
	line.set_ydata(dat)
	ax.set_xlim(0, len(dat))
	#ax.set_ylim(np.min(dat), np.max(dat))
	ax.set_ylim(0, 1)
	ax.set_title(str(pos))
	fig.canvas.draw()
	
fig, ax = plt.subplots()
fig.canvas.mpl_connect('key_press_event', press)

line, = ax.plot([], [])
plt.show()

