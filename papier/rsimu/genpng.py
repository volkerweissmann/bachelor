#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)

def makesingle(num):
	plt.clf()

	
	data  = {}
	matrix = np.loadtxt(fname="out/"+str(num)+".out", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
	    data[i+1] = matrix[:,i]
	#data[4] = np.sin(data[1]) so etwas geht, data[1]**3 geht auch
	#plt.title("Titel $\sigma_i$")
	#plt.figure(figsize=(20,10))
	plt.xlabel("x")
	plt.ylabel("p(x)")
	#plt.xscale('log')
	#plt.grid(True)
	#plt.axis([0, 3, 1, 9]) x-Achse von 0 bis 3, y-Achse von 1 bis 9
	#a = np.arange(-5.0, 5.0, 0.01) plt.plot(a, np.sin(a), "r-")

	plt.plot(data[1],data[2], label="Numerisch", linestyle="-", zorder=0, color="black")
	plt.plot(data[1],data[3], label="Ordnung 0", linestyle="--", dashes=(1, 2, 0, 0), color=plt.cm.plasma(0.8))
	plt.plot(data[1],data[4], label="Ordnung 1", linestyle="--", dashes=(1, 2, 0, 0), zorder=10, color=plt.cm.plasma(0.5))
	plt.plot(data[1],data[5], label="Ordnung 2", linestyle="--", dashes=(1, 2, 0, 1), zorder=20, color=plt.cm.plasma(0.3))
	plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True)
	#plt.legend(framealpha=1,loc='center left',bbox_to_anchor=(1, 0.5))
	if num < 10:
		plt.savefig("ani/00"+str(num)+".png")
	elif num < 100:
		plt.savefig("ani/0"+str(num)+".png")
	else:
		plt.savefig("ani/"+str(num)+".png")
	
for i in range(0,400):
	makesingle(i)

#ffmpeg -framerate 10 -i ani/%03d.png output.mp4