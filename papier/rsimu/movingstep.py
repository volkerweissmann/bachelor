#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)

data  = {}
matrix = np.loadtxt(fname="singles/movingstep.dat", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
data[0] = np.array(list(range(0,len(matrix))))
for i in range(0,len(matrix[0])):
    data[i+1] = matrix[:,i]
#data[4] = np.sin(data[1]) so etwas geht, data[1]**3 geht auch
#plt.title("Titel $\sigma_i$")
#plt.figure(figsize=(20,10))
plt.xlabel("x")
plt.ylabel("p(x)")
#plt.xscale('log')
#plt.grid(True)
#plt.axis([0, 3, 1, 9]) x-Achse von 0 bis 3, y-Achse von 1 bis 9
#a = np.arange(-5.0, 5.0, 0.01) plt.plot(a, np.sin(a), "r-")

plt.plot(np.arange(-200, 200+1, 1),data[1][500-200:500+200+1], linestyle="--", label="Numerisch", zorder=100, alpha=1)
plt.plot(np.arange(-200, 200+1, 1),data[2][500-200:500+200+1], label="Analytisch", alpha=1)
plt.legend(framealpha=1)
plt.savefig("movingstep.pdf")