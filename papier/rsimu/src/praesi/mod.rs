use ::*;
fn construct_evolve_operator_work(dim: usize, D: f64, work: &Fn(usize,usize)-> f64) -> MatrixF64 {
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		//per(dim,i)
		let mut ci = D*( -work(per(dim,i+1),per(dim, i  ))/2.).exp(); //c_i
		let mut ai = D*(  work(per(dim,i  ),per(dim, i-1))/2.).exp(); //a_{i}
		set_periodic(&mut mat, i+1, i, ci);
		set_periodic(&mut mat, i-1, i, ai);
	}
	for i in 0..dim as i32 {
		let temp = 1.-get_periodic(&mat, i+1,i)-get_periodic(&mat, i-1,i);
		//assert!(temp > -1.);
		set_periodic(&mut mat, i, i, temp);
	}
	for y in 0..dim as i32 {
		let mut sum = 0.;
		for x in 0..dim as i32 {
			sum += get_periodic(&mat, x,y);
		}
		assert!((1.-sum).abs() < 0.001);
	}
	return mat;
}
/// gets the bit at position `n`. Bits are numbered from 0 (least significant) to 31 (most significant).
fn get_bit_at(input: usize, n: usize) -> bool {
	input & (1 << n) != 0
}
const fn num_bits<T>() -> usize { std::mem::size_of::<T>() * 8 }
fn simulate(mut p: VectorF64, dim: usize, D: f64, work: &Fn(usize,usize)-> f64, write: &Fn(usize, f64, & VectorF64), steplen: usize) {
	let mut mats: Vec<MatrixF64> = Vec::new();
	mats.push( construct_evolve_operator_work(dim, D, work) );
	println!("constructed the single evolution operator");
	let log =  num_bits::<usize>() as i32 - steplen.leading_zeros() as i32 - 1; //https://users.rust-lang.org/t/logarithm-of-integers/8506/5
	assert!(log >= 0);
	let log = log as usize;
	for _i in 0..log {
		let mut next = MatrixF64::new(dim, dim).expect("Allocation failed");
		dgemm(NoTrans, NoTrans, 1., &mats[mats.len()-1], &mats[mats.len()-1], 0., &mut next);
		mats.push(next);
	}
	let mut stepmat = MatrixF64::new(dim, dim).expect("Allocation failed");
	stepmat.set_identity();
	for i in 0..(log+1) {
		let temp = stepmat.clone().expect("clone failed");
		if get_bit_at(steplen, i) {
			dgemm(NoTrans, NoTrans, 1., &mats[i], &temp   , 0., &mut stepmat);
		}
	}
	println!("constructed the power evolution operator");
	let mut vecswap = VectorF64::new(dim).expect("Allocation failed");

	for i in 0..200+1 {		
		write(2*i, steplen as f64*(0.+2.*i as f64), &p);
		dgemv(Trans, 1., &stepmat, &p, 0., &mut vecswap);

		write(2*i+1, steplen as f64*(1.+2.*i as f64), &vecswap);
		dgemv(Trans, 1., &stepmat, &vecswap, 0., &mut p);
	}
}
const m: usize = 4;
fn gen_order(D: f64, xis: [i32;m+1], Wis: [f64; m], xs: i32, order: i32) -> Box<dyn Fn(i32,f64) -> f64> {
	let mut fis: [Vec<(f64, i32, i32)>; m] = Default::default();
	for i in 0..m{
		if xis[i] <= xs && xs < xis[i+1] {
			fis[i].push((1.,0,1));
			fis[i].push((0.,1,-1));
		} else{
			fis[i].push((0.,0,  1));
			fis[i].push((0.,1,-1));
		}
	}
	let getat = |fi: & Vec<(f64, i32, i32)>, n:i32,s:i32| -> f64 {
		for pair in fi.iter() {
			if pair.1 == n && pair.2 == s {
				return pair.0;
			}
		}
		println!("{} {}", n, s);
		panic!("not found");
	};
	for n in 0..order as i32 {
		for i in 0..m {
			let p1 = per(m, i as i32 +1);
			let m1 = per(m, i as i32 -1);
			// man beachte, dass h(V) als tanh(beta V/2) definiert ist
			//diese beiden Gleichungen erhält man mit z=xs+na
			let valupper = ((Wis[p1]/2.).tanh()+1.) * getat( &fis[p1], 0+n, 1 )
				+ (Wis[p1]/2.).tanh()           * getat( &fis[i] , 1-n, -1);
			fis[i].push( ( valupper ,1+n, 1));
			let vallower = -((Wis[i]/2.).tanh()-1.) * getat( &fis[m1], 1-n, -1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i], 0+n,   1 );
			fis[i].push( ( vallower ,0-n,-1));
			//diese beiden Gleichungen erhält man mit z=a-xs+na
			let valtup = ((Wis[p1]/2.).tanh()+1.)  * getat( &fis[p1], 1+n, -1 )
				+ (Wis[p1]/2.).tanh()          * getat( &fis[i],  -n,   1 );
			fis[i].push( (valtup,  2+n, -1));
			let valtlow = -((Wis[i]/2.).tanh()-1.)  * getat( &fis[m1], -n, 1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i],  1+n, -1 );
			fis[i].push( (valtlow, -1-n, 1));
		}
	}
	return Box::new(move |x,t: f64| {
		let sq = 1./(4.*D*t*PI).sqrt();
		for i in 0..m {
			if xis[i] <= x && x < xis[i+1] {
				let mut ret = 0.;
				for pair in fis[i].iter() {
					let z = 200*(i as i32 + pair.1) + 50*pair.2;
					ret += pair.0 * (-(x as f64 - z as f64).powi(2)/(4.*D*t)).exp();
				}
				return sq*ret;
			}
		}
		//(-(x-z-force*D*t).powi(2)/(4.*D*t)).exp()
		return 0.;
	});
}
pub fn simushow() {
	let D: f64 = 0.2;
	let F0: f64 = 0.;//-0.02;
	let xis: [i32; m+1] = [0, 200, 400, 600, 800];
	let Wis: [f64; m] = [1., 1., -1., -2.];

	let xs = 250;

	



	
	let dim = xis[xis.len()-1]-xis[0];
	assert!(dim > 0);
	let dim = dim as usize;
	let work = |x1,x2| -> f64 {
		for i in 0..m {
			if x1 as i32 == xis[i]-xis[0] && x2 == per(dim, xis[i] - xis[0] -1) {
				return Wis[i]-F0;
			}
		}
		return 0.-F0;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set((xs-xis[0]) as usize, 1.);

	let ord_0 = gen_order(D, xis, Wis, xs, 0);
	let ord_1 = gen_order(D, xis, Wis, xs, 1);
	let ord_2 = gen_order(D, xis, Wis, xs, 2);
	
	let write = |i, t: f64, prob: &VectorF64| {
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		for j in 0..prob.len() {
			let x = xis[0] + j as i32;
			dat.write(format!
				  ("{} {} {} {} {}\n"
				   , x
				   , prob.get(j)
  				   , ord_0(x,t)
				   , ord_1(x,t)
				   , ord_2(x,t)
			).as_bytes()).expect("unable to write");
		}
	};
	simulate(p, dim, D, &work, &write, 1024);
	let strg = "
pos=0

bind 'k' \"isexist= system(sprintf('file out/%i.out | grep cannot',pos+1));if(strlen(isexist) == 0) pos=pos+1; plot for [j=2:10] sprintf('out/%i.out',pos) u 1:j w l title sprintf('%i %i',pos,j)
bind 'j' \"isexist= system(sprintf('file out/%i.out | grep cannot',pos-1));if(strlen(isexist) == 0) pos=pos-1; plot for [j=2:10] sprintf('out/%i.out',pos) u 1:j w l title sprintf('%i %i',pos,j)

plot for [j=2:10] sprintf('out/%i.out',pos) u 1:j w l title sprintf('%i %i',pos,j)
pause -1
";
	//println!("{}", strg);
	let mut gp = File::create("gen.plt").expect("unable to create file");
	gp.write(strg.as_bytes()).expect("unable to write");
	
	/*let output = std::process::Command::new("gnuplot")
		.args(&["-e",strg])
		.output().expect("unable to run gnuplot");*/
	/*drop(gp);
	let output = std::process::Command::new("gnuplot")
		.args(&["gen.plt"])
		.output().expect("unable to run gnuplot");
	println!("{:?}", output);
	if !output.status.success() {
		panic!("Gnuplots returned with a non zero exit state");
	}*/
}