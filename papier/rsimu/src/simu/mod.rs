use ::*;
fn construct_timeder_operator_work(dim: usize, D: f64, work: &Fn(usize,usize)-> f64) -> MatrixF64 {
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		//per(dim,i)
		let mut ci = D*( -work(per(dim,i+1),per(dim, i  ))/2.).exp(); //c_i
		let mut ai = D*(  work(per(dim,i  ),per(dim, i-1))/2.).exp(); //a_{i}
		set_periodic(&mut mat, i+1, i, ci);
		set_periodic(&mut mat, i-1, i, ai);
	}
	for i in 0..dim as i32 {
		let temp = -get_periodic(&mat, i+1,i)-get_periodic(&mat, i-1,i);
		//assert!(temp > -1.);
		set_periodic(&mut mat, i, i, temp);
	}
	for y in 0..dim as i32 {
		let mut sum = 0.;
		for x in 0..dim as i32 {
			sum += get_periodic(&mat, x,y);
		}
		assert!((sum).abs() < 0.001);
	}
	return mat;
}
pub fn construct_evolve_operator_work(dim: usize, D: f64, work: &Fn(usize,usize)-> f64) -> MatrixF64 {
	let mut mat = construct_timeder_operator_work(dim, D, work);
	for i in 0..dim as usize {
		let temp = mat.get(i,i);
		mat.set(i,i, 1.+temp);
	}
	return mat;
}
/// gets the bit at position `n`. Bits are numbered from 0 (least significant) to 31 (most significant).
fn get_bit_at(input: usize, n: usize) -> bool {
	input & (1 << n) != 0
}
const fn num_bits<T>() -> usize { std::mem::size_of::<T>() * 8 }
pub fn simulate(mut p: VectorF64, dim: usize, D: f64, work: &Fn(usize,usize)-> f64, write: &Fn(usize, f64, & VectorF64), steplen: usize) {
	let mut mats: Vec<MatrixF64> = Vec::new();
	mats.push( construct_evolve_operator_work(dim, D, work) );
	println!("constructed the single evolution operator");
	let log =  num_bits::<usize>() as i32 - steplen.leading_zeros() as i32 - 1; //https://users.rust-lang.org/t/logarithm-of-integers/8506/5
	assert!(log >= 0);
	let log = log as usize;
	for _i in 0..log {
		let mut next = MatrixF64::new(dim, dim).expect("Allocation failed");
		dgemm(NoTrans, NoTrans, 1., &mats[mats.len()-1], &mats[mats.len()-1], 0., &mut next);
		mats.push(next);
	}
	let mut stepmat = MatrixF64::new(dim, dim).expect("Allocation failed");
	stepmat.set_identity();
	for i in 0..(log+1) {
		let temp = stepmat.clone().expect("clone failed");
		if get_bit_at(steplen, i) {
			dgemm(NoTrans, NoTrans, 1., &mats[i], &temp   , 0., &mut stepmat);
		}
	}
	println!("constructed the power evolution operator");
	let mut vecswap = VectorF64::new(dim).expect("Allocation failed");

	for i in 0..200+1 {
		dgemv(Trans, 1., &stepmat, &p, 0., &mut vecswap);
		write(i, steplen as f64*(1.+2.*i as f64), &p);
		
		dgemv(Trans, 1., &stepmat, &vecswap, 0., &mut p);
		write(i, steplen as f64*(2.+2.*i as f64), &p);
	}
}

pub fn evolve(p: VectorF64, dim: usize, D: f64, work: &Fn(usize,usize)-> f64, steplen: usize) -> VectorF64 {
	let mut mats: Vec<MatrixF64> = Vec::new();
	mats.push( construct_evolve_operator_work(dim, D, work) );
	println!("constructed the single evolution operator");
	let log =  num_bits::<usize>() as i32 - steplen.leading_zeros() as i32 - 1; //https://users.rust-lang.org/t/logarithm-of-integers/8506/5
	assert!(log >= 0);
	let log = log as usize;
	for _i in 0..log {
		let mut next = MatrixF64::new(dim, dim).expect("Allocation failed");
		dgemm(NoTrans, NoTrans, 1., &mats[mats.len()-1], &mats[mats.len()-1], 0., &mut next);
		mats.push(next);
	}
	println!("constructed the power evolution operators");
	let mut stepmat = MatrixF64::new(dim, dim).expect("Allocation failed");
	stepmat.set_identity();
	for i in 0..(log+1) {
		let temp = stepmat.clone().expect("clone failed");
		if get_bit_at(steplen, i) {
			dgemm(NoTrans, NoTrans, 1., &mats[i], &temp   , 0., &mut stepmat); //PERFORMANCE TODO mit matrix vektor multiplikation geht das schneller
		}
	}
	println!("constructed the steplen evolution operator");
	let mut vecswap = VectorF64::new(dim).expect("Allocation failed");
	dgemv(Trans, 1., &stepmat, &p, 0., &mut vecswap);
	return vecswap;
}
