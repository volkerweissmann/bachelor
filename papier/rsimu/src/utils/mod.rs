use ::*;

macro_rules! dout {
	($name:ident) => {
		println!("{}:{} {}= {:?}", file!(), line!(), stringify!($name), $name);
	};
}

pub fn per(dim: usize, mut i: i32) -> usize{
	i = i % dim as i32;
	if i < 0 {
		i += dim as i32;
	}
	assert!(i >= 0);
	assert!((i as usize) < dim);
	return i as usize;
}
//same as MatrixF64::set(self, y,x,value) but for periodic indices and with x,y swapped
pub fn set_periodic(mat: &mut rgsl::MatrixF64, mut x: i32, mut y: i32, value: f64) {//#TODO: macht der ne deep copy von mat?
	x = x % mat.size1() as i32;
	y = y % mat.size2() as i32;
	if x < 0 {
		x += mat.size1() as i32;
	}
	if y < 0 {
		y += mat.size2() as i32;
	}
	assert!(x >= 0);
	assert!(y >= 0);
	assert!((x as usize) < mat.size1());
	assert!((y as usize) < mat.size2());
	mat.set(y as usize, x as usize, value);
}
//same as MatrixF64::get(self, y,x,value) but for periodic indices and with x,y swapped
pub fn get_periodic(mat: &rgsl::MatrixF64, mut x: i32, mut y: i32) -> f64 {
	x = x % mat.size1() as i32;
	y = y % mat.size2() as i32;
	if x < 0 {
		x += mat.size1() as i32;
	}
	if y < 0 {
		y += mat.size2() as i32;
	}
	assert!(x >= 0);
	assert!(y >= 0);
	assert!((x as usize) < mat.size1());
	assert!((y as usize) < mat.size2());
	return mat.get(y as usize, x as usize);
}
pub fn max_diff_mat(a: &rgsl::MatrixF64, b: &rgsl::MatrixF64) -> f64 {
	assert!(a.size1() == b.size1());
	assert!(a.size2() == b.size2());
	let mut max = 0.0;
	for x in 0..a.size1() {
		for y in 0..a.size2() {
			let diff = (a.get(y,x)-b.get(y,x)).abs();
			if diff > max {
				max = diff;
			} 
		}
	}
	return max;
}
pub fn max_diff_vec(a: &rgsl::VectorF64, b: &rgsl::VectorF64) -> f64 {
	assert!(a.len() == b.len());
	let mut max = 0.0;
	for x in 0..a.len() {
		let diff = (a.get(x)-b.get(x)).abs();
		if diff > max {
			max = diff;
		} 
	}
	return max;
}
pub fn sec_force<'a>(secs: &'a Vec<(f64, usize)>) -> impl Fn(usize, usize) -> f64 + 'a{
	let closure = move |dim: usize, x: usize| -> f64 {
		assert!(x < dim);
		let mut sum = 0;
		for f in secs.iter() {
			sum += f.1;
			if x < sum {
				return f.0;
			}
		}
		panic!("x does seem to be at least as large as dimarg");
	};
	return closure;
}
pub fn plotA(y: &VectorF64) {
	let mut dat = File::create("gnu.out").expect("unable to create file");
	for i in 0..y.len() {
		dat.write(format!("{}\n", y.get(i)).as_bytes()).expect("unable to write");
	}
	dat.sync_all().expect("unable to sync");
	let _output = std::process::Command::new("gnuplot")
		.args(&["-e", "plot 'gnu.out' with lines; pause mouse close"])
		.output().expect("unable to run gnuplot");
	
	//https://unix.stackexchange.com/questions/257679/how-to-keep-gnuplot-x11-graph-window-open-until-manually-closed
}
pub fn plotB(y1: &VectorF64, y2: &VectorF64) {
	let mut dat1 = File::create("gnu1.out").expect("unable to create file");
	for i in 0..y1.len() {
		dat1.write(format!("{}\n", y1.get(i)).as_bytes()).expect("unable to write");
	}
	dat1.sync_all().expect("unable to sync");
	let mut dat2 = File::create("gnu2.out").expect("unable to create file");
	for i in 0..y2.len() {
		dat2.write(format!("{}\n", y2.get(i)).as_bytes()).expect("unable to write");
	}
	dat2.sync_all().expect("unable to sync");
	let output = std::process::Command::new("gnuplot")
		.args(&["-e", "plot 'gnu1.out' with lines, 'gnu2.out' with lines; pause mouse close"])
		.output().expect("unable to run gnuplot");
	println!("{:?}", output);
	
	//https://unix.stackexchange.com/questions/257679/how-to-keep-gnuplot-x11-graph-window-open-until-manually-closed
}
pub fn plotC(y: &Vec::<f64>) {
	let mut dat = File::create("gnu.out").expect("unable to create file");
	for v in y.iter() {
		dat.write(format!("{}\n", v).as_bytes()).expect("unable to write");
	}
	dat.sync_all().expect("unable to sync");
	let _output = std::process::Command::new("gnuplot")
		.args(&["-e", "plot 'gnu.out' with lines; pause mouse close"])
		.output().expect("unable to run gnuplot");
	
	//https://unix.stackexchange.com/questions/257679/how-to-keep-gnuplot-x11-graph-window-open-until-manually-closed
}