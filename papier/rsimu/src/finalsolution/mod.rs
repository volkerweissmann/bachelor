use ::*;
pub fn compare_final() {
	let dim = 1000; //dimension
	let xof = dim/2; //x offset

	let D = 0.2;
	let F_0 = 0.02;
	let F_1 = F_0/4.;
	let V_0 = 1.;

	let x_a = 0;
	let x_s = dim/5;
	let pot = |x| {
		if x > x_s + xof {
			return V_0-F_0*x as f64;
		}
		return 0.-F_0*x as f64;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(x_a+xof, 1.);

	let beta = 1.;
	let x_a = x_a as f64;
	let x_s = x_s as f64;
	let anal = |x: f64, t1:f64| -> f64 {
		if x < x_s {
			let mut ret = 0.;
			let z = 2.*x_s-x_a;
			let delt = std::f64::consts::E.powf((-1.)*((2.*F_1)*x_a))/((1_f64/2.0)*V_0*beta).tanh();
			ret += 1./(PI*t1).sqrt()*(-(x-z).powi(2)/t1+2.*F_1*x-F_1*F_1*t1).exp()*delt;
			ret += quadrature::integrate(|z|->f64{
				let func = 2.*F_1*std::f64::consts::E.powf((-1.)*((2.*F_1)*x_a))*(1. + ((1_f64/2.0)*V_0*beta).tanh().recip())*((2.*F_1)*(x_a + (-1.)*(2.*x_s) + z)).exp();
				1./(PI*t1).sqrt()*(-(x-z).powi(2)/t1+2.*F_1*x-F_1*F_1*t1).exp()*func
			}, 2.*x_s-x_a, 1000., 1e-9).integral;
			return ret;
		}
		let mut ret = 0.;
		
		let z = x_a;
		let delt = 2.*std::f64::consts::E.powf((-1.)*((2.*F_1)*x_a))/(std::f64::consts::E.powf(V_0*beta) + 1.);
		ret += 1./(PI*t1).sqrt()*(-(x-z).powi(2)/t1+2.*F_1*x-F_1*F_1*t1).exp()*delt;
		ret += quadrature::integrate(|z|->f64{
			let func = 4.*F_1*std::f64::consts::E.powf((-1.)*((2.*F_1)*x_a))*(1. - std::f64::consts::E.powf(V_0*beta))*(2.*F_1*(1. - std::f64::consts::E.powf(V_0*beta))*((-1.)*x_a + z)/(std::f64::consts::E.powf(V_0*beta) + 1.)).exp()/(std::f64::consts::E.powf(V_0*beta) + 1.).powi(2);
			1./(PI*t1).sqrt()*(-(x-z).powi(2)/t1+2.*F_1*x-F_1*F_1*t1).exp()*func
		}, x_a,1000., 1e-9).integral;
		
		return 0.;
	};
	let write = |i, t: f64, prob: &VectorF64| {
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		
		let t1 = 4.*D*t;
		
		for j in 0..prob.len() {
			let analval = anal(j as f64 - xof as f64, t1);
			dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
		}
	};
	stage5::simulate_mat_vec_mult(p, dim, D, &pot, &write);
}