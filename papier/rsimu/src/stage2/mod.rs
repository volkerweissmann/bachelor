use ::*;

//D = T\mu

// SI: B := -\mu F(x,t) + D\partial_x
// B_si = B_nat *D/dx
// force_nat(xi) = = force_si(xi*dx)* dx*\beta
fn construct_B_nat(dim: usize, force_nat: &Fn(usize, usize)-> f64) -> MatrixF64 {
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		set_periodic(&mut mat, i, i+1, -0.5);
		set_periodic(&mut mat, i, i-1,  0.5);
		set_periodic(&mut mat, i, i, -force_nat(dim, i as usize));
	}
	return mat;
}
//return p and j_nat = j*dx^2/D  ; sum(p) = 1
pub fn calc_ness(dim: usize, force_nat: &Fn(usize, usize)-> f64) -> (VectorF64, f64) {
	let mat = construct_B_nat(dim, force_nat);
	let mut vec = VectorF64::new(dim).expect("allocation failed");
	vec.set_all(1.0);
	println!("calculating ness via householder transformations...");
	rgsl::linear_algebra::HH_svx(mat, &mut vec);
	println!("finished the householder transformations.");
	// B_si*(-p/j) = 1
	// D/dx*B_nat*(-p/j) = 1
	// B_nat*(-p*D/j/dx) = 1 -> vec enthält -p*D/j/dx -> sum(vec) = int(vec)/dx = -D/j/dx^2
	let mut m_div_j_nat = 0.0; // -D/j/dx^2
	for i in 0..dim {
		m_div_j_nat += vec.get(i);
	}
	vec.scale(1.0/m_div_j_nat);
	return (vec, -1.0/m_div_j_nat);
}
fn forcetest_1(x: f64, _t: f64) -> f64{
	return x.sin();
}
pub fn compare_stages_1_2() {
	let mut max = 0.0;
	for &dx in [0.3, 17.0].into_iter() {
		for force in [forcetest_1].into_iter() {
			for &mu in [23.0, 47.0].into_iter() {
				for &D in [0.7, 1.9].into_iter() {
					let closure = |_dim: usize, xi: usize| -> f64 {
						return force(xi as f64*dx, 3.0)*dx*mu/D;
					};
					for &dim in [10, 100].into_iter() {
						let B_nat = stage2::construct_B_nat(dim, &closure);

						let mut B_si = stage1::construct_B_si(dim, dx, force, mu, D, 7.0);
						B_si.scale(dx/D);
						let diff = max_diff_mat(&B_si, &B_nat);
						if diff > max {
							max = diff;
						}
					}
				}
			}
		}
	}
	println!("compared stages 1 and 2: log10(maxdiff)= {:.0}", max.log10());
}
