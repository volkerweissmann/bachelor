use ::*;

pub fn generate_plots() {
	let D = 0.2;
	let V0 = 1.;
	let force = -0.02;

	let dim = 1000;
	let x0 = dim/2;
	let xs = 50;
	let pot = |x| {
		if x >= x0 {
			return V0-force*x as f64;
		}
		return 0.-force*x as f64;
	};
	let _work = |x1: usize,x2: usize| {
		if x1 == x0 && x2 == x0-1 {
			return V0-force;
		}
		return -force;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(x0+xs as usize, 1.);
	let xs = xs as f64;

	let tanh = ((V0).exp()-1.)/((V0).exp()+1.);

	let anal = |x: f64, t:f64| -> f64 {
		let sq = 1./(4.*D*t*PI).sqrt();
		let ar = -force/2.*(1.-tanh);
		let al = force/2.*(1.+tanh);
		/*let br = -(x+xs-D*force*t);
		let bl = x-xs-D*force*t;
		let c = 4.*D*t;*/
		if x >= 0. {
			sq*(-(x-xs-force*D*t).powi(2)/(4.*D*t)).exp()
				- sq*tanh * (-(x+xs-D*force*t).powi(2)/(4.*D*t)).exp() * (-xs*force).exp()
				+ force/4.*tanh*(1.-tanh)* (-force*xs).exp()
				* ( -ar*( (-1.-tanh)*force/2.*D*t +x + xs )  ).exp() * (1.+erf((force*tanh*D*t -x-xs)/(4.*D*t).sqrt()))
				//* ( ar*ar*c/4. + ar*br  ).exp() * (1.+erf((ar*c+2.*br)/2./c.sqrt()))
			/* *quadrature::integrate(|z|->f64{
				return  (ar*z).exp()
			 * (-(z-br).powi(2)/(c)).exp();
		}, 0., 500., 1e-9).integral*/
			/*+ quadrature::integrate(|z|->f64{
				return  (force/2.*tanh*(-z-xs)).exp()*(1.-tanh)*tanh* force/2.*(-force/2.*xs).exp()  
			 *(force/2.*z).exp()* (-(x-z-D*force*t).powi(2)/(4.*D*t)).exp();
		}, -500., -xs, 1e-9).integral*/
		} else {
			0.
				+ sq*(-(x-xs-D*force*t).powi(2)/(4.*D*t)).exp() * (1.+ tanh)
				+ force/4.*tanh*(1.+tanh)*
				( al*( (tanh-1.)*force/2.*D*t + x-xs)  ).exp() * (1.+erf( (x-xs +  tanh*force*D*t )/(4.*D*t).sqrt()))
				//( al*al*c/4. + al*bl  ).exp() * (1.+erf((al*c+2.*bl)/2./c.sqrt()))
			/*quadrature::integrate(|z|->f64{
				return (z*al).exp()
			 * (-(z-bl).powi(2)/(c)).exp();
		}, 0., 500., 1e-9).integral*/
			/*+ quadrature::integrate(|z|->f64{
				return ((z-xs)*force/2.*tanh).exp() *force/2.*tanh*(1.+tanh)*(-force/2.*xs).exp()
			 *(force/2.*z).exp()* (-(x-z-D*force*t).powi(2)/(4.*D*t)).exp();
		}, xs, 500., 1e-9).integral*/
		}

	};
	/*let t = 10000;
	let prob = simu::evolve(p, dim, D, &work, t);
	
	let mut dat = File::create("singles/movingstep.dat").expect("unable to create file");
	for j in 0..prob.len() {
		let analval = anal((j as f64) - x0 as f64 , t as f64);
		dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
	}
	drop(dat);*/
	
	/*let output = std::process::Command::new("gnuplot")
		.args(&["-e", "
set term pdf;
set output 'movingstep.pdf';

set xlabel 'x';
set ylabel 'p(x)';

set xrange [-200:200];

plot 'singles/movingstep.dat' using ($0-500):1 t 'Numerisch' with lines, 'singles/movingstep.dat' using ($0-500):2 t 'Analytisch' with lines;
"])
		.output().expect("unable to run gnuplot");
	println!("{:?}", output);
	if !output.status.success() {
		panic!("Gnuplots returned with a non zero exit state");
	}*/

	

	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(x0+xs as usize, 1.);
	
	//return;
	let write = |i, t: f64, prob: &VectorF64| {
		if i == 15 {
			println!("{}", t);
		}
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");

		for j in 0..prob.len() {
			let analval = anal((j as f64) - x0 as f64 , t);//*(4.*V0*F0).exp();
			dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
		}
		
	};
	stage5::simulate_mat_vec_mult(p, dim, D, &pot, &write);

}