use ::*;

pub fn anal_ness_sec_const(forces: &Vec<(f64, usize)>) -> VectorF64 { //doesn't work if j is zero or at least on force is zero
	for f in forces.iter() {
		assert!(f.1 > 0);
	}
	let lf = forces.len();
	let mut sum = 0.0;
	for o in 0..lf {
		let mut next = {
			if o != lf-1 {
				1.0/forces[o].0 - 1.0/forces[o+1].0
			} else {
				1.0/forces[o].0 - 1.0/forces[0].0
			}
		};
		for i in o+1..lf {
			next *= (forces[i].0*forces[i].1 as f64).exp()
		}
		sum += next;
	}
	let mut prod1 = 1.0;
	for f in forces.iter() {
		prod1 *= (f.0*f.1 as f64).exp() // e^{beta*f*delta x}
	}
	let mut cs = Vec::new();
	cs.push(sum/(1.0-prod1));
	for i in 0..lf-1 {
		let csl = cs[i];
		cs.push( (1.0/forces[i].0 - 1.0/forces[i+1].0)+(forces[i].0*forces[i].1 as f64).exp()*csl); //c_{i+1} = d_i + f_i c_i
	}	
	let mut integral = 0.0; //=1/j
	for i in 0..lf {
		integral += forces[i].1 as f64/forces[i].0 + cs[i]/forces[i].0*((forces[i].0*forces[i].1 as f64).exp()-1.0);
	}
	let mut dim = 0;
	for f in forces.iter() {
		dim += f.1;
	}
	let mut p = VectorF64::new(dim as usize).expect("allocation failed");
	let mut index = 0;
	for fi in 0..lf {
		let startindex = index;
		for _i in 0..forces[fi].1 {
			p.set(index, (cs[fi]*(forces[fi].0*(index-startindex)as f64).exp()+1.0/forces[fi].0)/integral);
			index += 1;
		}
	}
	return p;
}
pub fn compare_stages_2_3() {
	let dimfac: usize = 100;
	let forces = vec![(1.1/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac), (-0.9/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac)];
	//let forces = vec![(1.1/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac), (-0.9/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac), (2.1/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac), (-1.9/dimfac as f64, dimfac), (0.1/dimfac as f64, dimfac)];
	//let forces = vec![(0.11, 10), (0.01, 10), (-0.09, 10), (0.01, 10)];
	let pa = anal_ness_sec_const(&forces);
	let mut dim: usize = 0;
	for f in forces.iter() {
		assert!(f.1 > 0);
		dim += f.1 as usize;
	}
	let pn = stage2::calc_ness(dim, &sec_force(&forces));
	plotB(&pa, &pn.0);
}
