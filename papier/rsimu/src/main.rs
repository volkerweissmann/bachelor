#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(unused_macros)]
#![allow(dead_code)]

extern crate statrs;
use statrs::function::erf::erf;
extern crate quadrature;
extern crate rgsl;
use rgsl::MatrixF64;
use rgsl::VectorF64;
//use rgsl::VectorView;
//use rgsl::blas::level1::dasum;
use rgsl::blas::level2::dgemv;
use rgsl::blas::level3::dgemm;
use rgsl::cblas::Transpose::NoTrans;
use rgsl::cblas::Transpose::Trans;
//use rgsl::types::integration::IntegrationWorkspace;

use std::fs::File;
//use std::io::prelude::*;
use std::io::Write;
use std::f64::consts::PI;

#[macro_use]
mod utils;
use utils::*;
mod stage1;
mod stage2;
mod stage3;
mod stage4;
mod stage5;
mod stage6;
mod periodic;

mod simu;
mod movingstep;
mod noforce;
mod praesi;


fn main() {
	praesi::simushow();
	movingstep::generate_plots();
	let times = [2000, 10000, 40000, 200000];
	for t in times.iter() {
		noforce::gen_num(*t);
		noforce::gen_order(0,*t);
		noforce::gen_order(1,*t);
		noforce::gen_order(2,*t);
	}
}


