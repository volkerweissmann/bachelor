use ::*;

pub fn construct_timeder_operator_old(dim: usize, D: f64, force: &Fn(usize, usize)-> f64) -> MatrixF64 {
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		set_periodic(&mut mat, i-1, i,    D-D*0.5*force(dim,i as usize));
		set_periodic(&mut mat, i, i,  -2.*D                            -D*0.5*(force(dim,per(dim,i+1))-force(dim,per(dim, i-1))));
		set_periodic(&mut mat, i+1, i,    D+D*0.5*force(dim,i as usize));
	}
	for y in 0..dim as i32 {
		let mut sum = 0.;
		for x in 0..dim as i32 {
			sum += get_periodic(&mat, x,y);
		}
		assert!((sum).abs() < 0.001);
	}
	return mat;
}
pub fn construct_timeder_operator_pot(dim: usize, D: f64, pot: &Fn(usize)-> f64) -> MatrixF64 {
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		let mut ci = D*(pot(i as usize)/2.-pot(per(dim,i+1))/2.).exp(); //c_i
		let mut aip1 = D*(-pot(i as usize)/2.+pot(per(dim,i+1))/2.).exp(); //a_{i+1}		
		if i == dim as i32 -1 {
			ci = 0.4;
			aip1 = 0.4;
		}
		set_periodic(&mut mat, i+1, i, ci);
		set_periodic(&mut mat, i, i+1, aip1);
		assert!(ci+aip1 < 1.);
		//set_periodic(&mut mat, i-1, i,    D-D*0.5*force(dim,i as usize));
		//set_periodic(&mut mat, i+1, i,    D+D*0.5*force(dim,i as usize));
	}
	for i in 0..dim as i32 {
		let temp = -get_periodic(&mat, i+1,i)-get_periodic(&mat, i-1,i);
		set_periodic(&mut mat, i, i, temp);
	}
	for y in 0..dim as i32 {
		let mut sum = 0.;
		for x in 0..dim as i32 {
			sum += get_periodic(&mat, x,y);
		}
		assert!((sum).abs() < 0.001);
	}
	return mat;
}
fn construct_evolve_operator_pot(dim: usize, D: f64, pot: &Fn(usize)-> f64) -> MatrixF64 {
	let mut mat = construct_timeder_operator_pot(dim, D, pot);
	for i in 0..dim as usize {
		let temp = mat.get(i,i);
		mat.set(i,i, 1.+temp);
	}
	return mat;
}
pub fn simulate_mat_vec_mult(mut p: VectorF64, dim: usize, D: f64, pot: &Fn(usize)-> f64, write: &Fn(usize, f64, & VectorF64)) {
	let mut mat = construct_evolve_operator_pot(dim, D, pot);
	println!("constructed the single evolution operator");
	let mut swap = MatrixF64::new(dim, dim).expect("Allocation failed");
	let mut ts = 1.;
	for i in 0..3 {
		dgemm(NoTrans, NoTrans, 1., &mat, &mat, 0., &mut swap);
		ts *= 2.;
		dgemm(NoTrans, NoTrans, 1., &swap, &swap, 0., &mut mat);
		ts *= 2.;
		println!("{}", i);
	}
	println!("constructed the power evolution operator");
	let mut vecswap = VectorF64::new(dim).expect("Allocation failed");

	let steplen = 4;
	for i in 0..200+1 {
		for _j in 0..steplen {
			dgemv(Trans, 1., &mat, &p, 0., &mut vecswap);
			dgemv(Trans, 1., &mat, &vecswap, 0., &mut p);
		}
		write(i, ts*2.*(i+1) as f64*steplen as f64, &p);
	}
}
