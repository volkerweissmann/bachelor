use ::*;
pub fn compare_periodic() {
	let D = 0.2;
	let F0 = 0.02;
	let F1 = F0/4.;

	let dim = 500; //dimension
	let xof = dim/2; //x offset
	let pot = |x| {
		return 0.-F0*x as f64;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(0+xof, 1.);
	
	let anal = |x: f64, t1:f64| -> f64 {
		let mut ret = 0.;
		for i in -100..101 {
			let r = dim as f64/2.;
			let z = 2.*r*i as f64;
			let delt = (-4.*F1*r*i as f64).exp();
			ret += 1./(PI*t1).sqrt()*(-(x-z).powi(2)/t1+2.*F1*x-F1*F1*t1).exp()*delt;
		}
		return ret;
	};
	let write = |i, t: f64, prob: &VectorF64| {
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		
		let t1 = 4.*D*t;
		
		for j in 0..prob.len() {
			let analval = anal(j as f64 - xof as f64, t1);
			dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
		}
	};
	stage5::simulate_mat_vec_mult(p, dim, D, &pot, &write);
}
/* BASIC TEMPLATE FOR ANAL SIMULATION COMPARISON:
pub fn compare() {
	let D = 0.2;
	let F0 = 0.02;
	let F1 = F0/4.;

	let dim = 1000; //dimension
	let xof = dim/2; //x offset
	let pot = |x| {
		return 0.-F0*x as f64;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(0+xof, 1.);
	
	let anal = |x: f64, t1:f64| -> f64 {
		return 0.;
	};
	let write = |i, t: f64, prob: &VectorF64| {
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		
		let t1 = 4.*D*t;
		
		for j in 0..prob.len() {
			let analval = anal(j as f64 - xof as f64, t1);
			dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
		}
	};
	stage5::simulate_mat_vec_mult(p, dim, D, &pot, &write);
}
*/