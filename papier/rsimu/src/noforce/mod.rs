use ::*;
pub fn gen_num(steplen: usize) {
	let D: f64 = 0.2;
	const m: usize = 4;
	let xa = 250;
	let xis: [usize; m+1] = [0, 200, 400, 600, 800];
	let Wis: [f64; m] = [1., 1., -1., -2.];
	//let Wis: [f64; m] = [1., -1., 1., 0.];
	//let Wis: [f64; m] = [0., 0., 1., 0.];	
	let dim = xis[xis.len()-1];
	let work = |x1,x2| -> f64 {
		for i in 0..m {
			if x1 == xis[i] && x2 == per(dim, xis[i] as i32 -1) {
				return Wis[i];
			}
		}
		return 0.;
	};
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(xa, 1.);
	let prob = simu::evolve(p, dim, D, &work, steplen);
	let mut dat = File::create(format!("singles/num_{}.dat", steplen).as_str()).expect("unable to create file");
	for j in 0..prob.len() {
		dat.write(format!("{}\n", prob.get(j)).as_bytes()).expect("unable to write");
	}
}
pub fn gen_order(order: usize, steplen: usize) {
	let D: f64 = 0.2;
	const m: usize = 4;
	let xis: [usize; m+1] = [0, 200, 400, 600, 800];
	let Wis: [f64; m] = [1., 1., -1., -2.];
	//let Wis: [f64; m] = [1., -1., 1., 0.];
	//let Wis: [f64; m] = [0., 0., 1., 0.];	
	let dim = xis[xis.len()-1];	
	let mut fis: [Vec<(f64, i32, i32)>; m] = Default::default();
	let xa = 250;
	for i in 0..m{
		if xis[i] <= xa && xa < xis[i+1] {
			fis[i].push((1.,0,1));
			fis[i].push((0.,1,-1));
		} else{
			fis[i].push((0.,0,  1));
			fis[i].push((0.,1,-1));
		}
	}
	let getat = |fi: & Vec<(f64, i32, i32)>, n:i32,s:i32| -> f64 {
		for pair in fi.iter() {
			if pair.1 == n && pair.2 == s {
				return pair.0;
			}
		}
		println!("{} {}", n, s);
		panic!("not found");
	};
	for n in 0..order as i32 {
		for i in 0..m {
			let p1 = per(m, i as i32 +1);
			let m1 = per(m, i as i32 -1);
			// man beachte, dass h(V) als tanh(beta V/2) definiert ist
			//diese beiden Gleichungen erhält man mit z=xs+na
			let valupper = ((Wis[p1]/2.).tanh()+1.) * getat( &fis[p1], 0+n, 1 )
				+ (Wis[p1]/2.).tanh()           * getat( &fis[i] , 1-n, -1);
			fis[i].push( ( valupper ,1+n, 1));
			let vallower = -((Wis[i]/2.).tanh()-1.) * getat( &fis[m1], 1-n, -1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i], 0+n,   1 );
			fis[i].push( ( vallower ,0-n,-1));
			//diese beiden Gleichungen erhält man mit z=a-xs+na
			let valtup = ((Wis[p1]/2.).tanh()+1.)  * getat( &fis[p1], 1+n, -1 )
				+ (Wis[p1]/2.).tanh()          * getat( &fis[i],  -n,   1 );
			fis[i].push( (valtup,  2+n, -1));
			let valtlow = -((Wis[i]/2.).tanh()-1.)  * getat( &fis[m1], -n, 1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i],  1+n, -1 );
			fis[i].push( (valtlow, -1-n, 1));
		}
	}
	let anal = |x,t: f64| {
		let sq = 1./(4.*D*t*PI).sqrt();
		for i in 0..m {
			if xis[i] <= x && x < xis[i+1] {
				let mut ret = 0.;
				for pair in fis[i].iter() {
					let z = 200*(i as i32 + pair.1) + 50*pair.2;
					ret += pair.0 * (-(x as f64 - z as f64).powi(2)/(4.*D*t)).exp();
				}
				return sq*ret;
			}
		}
		//(-(x-z-force*D*t).powi(2)/(4.*D*t)).exp()
		return 0.;
	};
	let mut dat = File::create(format!("singles/anal_{}_{}.dat", order, steplen).as_str()).expect("unable to create file");
	for j in 0..dim {
		let analval = anal(j,steplen as f64);
		dat.write(format!("{}\n", analval).as_bytes()).expect("unable to write");
	}
}
pub fn gen_pic(order: usize, steplen: usize) {
	let D: f64 = 0.2;
	const m: usize = 4;
	let xis: [usize; m+1] = [0, 200, 400, 600, 800];
	let Wis: [f64; m] = [1., 1., -1., -2.];
	//let Wis: [f64; m] = [1., -1., 1., 0.];
	//let Wis: [f64; m] = [0., 0., 1., 0.];	
	let dim = xis[xis.len()-1];	
	let work = |x1,x2| -> f64 {
		for i in 0..m {
			if x1 == xis[i] && x2 == per(dim, xis[i] as i32 -1) {
				return Wis[i];
			}
		}
		return 0.;
	};
	let mut fis: [Vec<(f64, i32, i32)>; m] = Default::default();
	let xa = 250;
	for i in 0..m{
		if xis[i] <= xa && xa < xis[i+1] {
			fis[i].push((1.,0,1));
			fis[i].push((0.,1,-1));
		} else{
			fis[i].push((0.,0,  1));
			fis[i].push((0.,1,-1));
		}
	}
	let getat = |fi: & Vec<(f64, i32, i32)>, n:i32,s:i32| -> f64 {
		for pair in fi.iter() {
			if pair.1 == n && pair.2 == s {
				return pair.0;
			}
		}
		println!("{} {}", n, s);
		panic!("not found");
	};
	for n in 0..order as i32 {
		for i in 0..m {
			let p1 = per(m, i as i32 +1);
			let m1 = per(m, i as i32 -1);
			// man beachte, dass h(V) als tanh(beta V/2) definiert ist
			//diese beiden Gleichungen erhält man mit z=xs+na
			let valupper = ((Wis[p1]/2.).tanh()+1.) * getat( &fis[p1], 0+n, 1 )
				+ (Wis[p1]/2.).tanh()           * getat( &fis[i] , 1-n, -1);
			fis[i].push( ( valupper ,1+n, 1));
			let vallower = -((Wis[i]/2.).tanh()-1.) * getat( &fis[m1], 1-n, -1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i], 0+n,   1 );
			fis[i].push( ( vallower ,0-n,-1));
			//diese beiden Gleichungen erhält man mit z=a-xs+na
			let valtup = ((Wis[p1]/2.).tanh()+1.)  * getat( &fis[p1], 1+n, -1 )
				+ (Wis[p1]/2.).tanh()          * getat( &fis[i],  -n,   1 );
			fis[i].push( (valtup,  2+n, -1));
			let valtlow = -((Wis[i]/2.).tanh()-1.)  * getat( &fis[m1], -n, 1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i],  1+n, -1 );
			fis[i].push( (valtlow, -1-n, 1));
		}
	}
	let anal = |x,t: f64| {
		let sq = 1./(4.*D*t*PI).sqrt();
		for i in 0..m {
			if xis[i] <= x && x < xis[i+1] {
				let mut ret = 0.;
				for pair in fis[i].iter() {
					let z = 200*(i as i32 + pair.1) + 50*pair.2;
					ret += pair.0 * (-(x as f64 - z as f64).powi(2)/(4.*D*t)).exp();
				}
				return sq*ret;
			}
		}
		//(-(x-z-force*D*t).powi(2)/(4.*D*t)).exp()
		return 0.;
	};
		
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(xa, 1.);

	let prob = simu::evolve(p, dim, D, &work, steplen);
	let mut dat = File::create("singles/temp.dat").expect("unable to create file");
	for j in 0..prob.len() {
		let analval = anal(j,steplen as f64);
		dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
	}
	drop(dat);
	let output = std::process::Command::new("gnuplot")
		.args(&["-e", format!("
set term pdf;
set output 'nof_{}_{}.pdf';

set xlabel 'x';
set ylabel 'p(x)';

plot 'singles/temp.dat' using 0:1 t 'Numerisch' with lines, 'singles/temp.dat' using 0:2 t 'Analytisch' with lines;
", order, steplen).as_str()])
		.output().expect("unable to run gnuplot");
	println!("{:?}", output);
	if !output.status.success() {
		panic!("Gnuplots returned with a non zero exit state");
	}
}
pub fn generate_plots() {
	let D: f64 = 0.2;
	const m: usize = 4;
	let xis: [usize; m+1] = [0, 200, 400, 600, 800];
	let Wis: [f64; m] = [1., 1., -1., -2.];
	//let Wis: [f64; m] = [-1., -1., 1., 2.];
	//let Wis: [f64; m] = [1., -1., 1., 0.];
	//let Wis: [f64; m] = [0., 0., 1., 0.];	
	let dim = xis[xis.len()-1];	
	let work = |x1,x2| -> f64 {
		for i in 0..m {
			if x1 == xis[i] && x2 == per(dim, xis[i] as i32 -1) {
				return Wis[i];
			}
		}
		return 0.;
	};
	/*let mat = simu::construct_evolve_operator_work(dim, D, &work);
	for x in 595..605 {
		for y in 595..605 {
			let num = format!("{:.2} ", mat.get(y,x));
			for _i in num.len()..6 {
				print!(" ");
			}
			print!("{}", num);
		}
		print!("\n");
	}
	stable:

 0.14  0.43  0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.00 
 0.43  0.14  0.43  0.00  0.00  0.00  0.00  0.00  0.00  0.00 
 0.00  0.43  0.14  0.43  0.00  0.00  0.00  0.00  0.00  0.00 
 0.00  0.00  0.43  0.14  0.43  0.00  0.00  0.00  0.00  0.00 
 0.00  0.00  0.00  0.43 -0.60  0.16  0.00  0.00  0.00  0.00 
 0.00  0.00  0.00  0.00  1.17  0.41  0.43  0.00  0.00  0.00 
 0.00  0.00  0.00  0.00  0.00  0.43  0.14  0.43  0.00  0.00 
 0.00  0.00  0.00  0.00  0.00  0.00  0.43  0.14  0.43  0.00 
 0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.43  0.14  0.43 
 0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.43  0.14 


unstable:

 0.12  0.44  0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.00 
 0.44  0.12  0.44  0.00  0.00  0.00  0.00  0.00  0.00  0.00 
 0.00  0.44  0.12  0.44  0.00  0.00  0.00  0.00  0.00  0.00 
 0.00  0.00  0.44  0.12  0.44  0.00  0.00  0.00  0.00  0.00 
 0.00  0.00  0.00  0.44 -0.64  0.16  0.00  0.00  0.00  0.00 
 0.00  0.00  0.00  0.00  1.20  0.40  0.44  0.00  0.00  0.00 
 0.00  0.00  0.00  0.00  0.00  0.44  0.12  0.44  0.00  0.00 
 0.00  0.00  0.00  0.00  0.00  0.00  0.44  0.12  0.44  0.00 
 0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.44  0.12  0.44 
 0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.00  0.44  0.12 
	return;*/

	let mut fis: [Vec<(f64, i32, i32)>; m] = Default::default();
	let xa = 250;
	for i in 0..m{
		if xis[i] <= xa && xa < xis[i+1] {
			fis[i].push((1.,0,1));
			fis[i].push((0.,1,-1));
		} else{
			fis[i].push((0.,0,  1));
			fis[i].push((0.,1,-1));
		}
	}
	let getat = |fi: & Vec<(f64, i32, i32)>, n:i32,s:i32| -> f64 {
		for pair in fi.iter() {
			if pair.1 == n && pair.2 == s {
				return pair.0;
			}
		}
		println!("{} {}", n, s);
		panic!("not found");
	};
	for n in 0..1 {
		for i in 0..m {
			let p1 = per(m, i as i32 +1);
			let m1 = per(m, i as i32 -1);
			// man beachte, dass h(V) als tanh(beta V/2) definiert ist
			//diese beiden Gleichungen erhält man mit z=xs+na
			let valupper = ((Wis[p1]/2.).tanh()+1.) * getat( &fis[p1], 0+n, 1 )
				+ (Wis[p1]/2.).tanh()           * getat( &fis[i] , 1-n, -1);
			fis[i].push( ( valupper ,1+n, 1));
			let vallower = -((Wis[i]/2.).tanh()-1.) * getat( &fis[m1], 1-n, -1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i], 0+n,   1 );
			fis[i].push( ( vallower ,0-n,-1));
			//diese beiden Gleichungen erhält man mit z=a-xs+na
			let valtup = ((Wis[p1]/2.).tanh()+1.)  * getat( &fis[p1], 1+n, -1 )
				+ (Wis[p1]/2.).tanh()          * getat( &fis[i],  -n,   1 );
			fis[i].push( (valtup,  2+n, -1));
			let valtlow = -((Wis[i]/2.).tanh()-1.)  * getat( &fis[m1], -n, 1 )
				- (Wis[i]/2.).tanh()           * getat( &fis[i],  1+n, -1 );
			fis[i].push( (valtlow, -1-n, 1));
		}
	}
	let anal = |x,t: f64| {
		let sq = 1./(4.*D*t*PI).sqrt();
		for i in 0..m {
			if xis[i] <= x && x < xis[i+1] {
				let mut ret = 0.;
				for pair in fis[i].iter() {
					let z = 200*(i as i32 + pair.1) + 50*pair.2;
					ret += pair.0 * (-(x as f64 - z as f64).powi(2)/(4.*D*t)).exp();
				}
				return sq*ret;
			}
		}
		//(-(x-z-force*D*t).powi(2)/(4.*D*t)).exp()
		return 0.;
	};
	let write = |i, t: f64, prob: &VectorF64| {
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		for j in 0..prob.len() {
			let analval = anal(j,t);
			dat.write(format!("{} {}\n", prob.get(j), analval).as_bytes()).expect("unable to write");
		}
	};
		
	let mut p = VectorF64::new(dim).expect("Allocation failed");
	p.set(xa, 1.);
	simu::simulate(p, dim, D, &work, &write, 512);
}