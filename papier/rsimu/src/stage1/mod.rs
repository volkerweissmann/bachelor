use ::*;

//Construct a matrix that represents derivation with periodic boundary conditions
fn construct_periodic_derivation(dim: usize, dx: f64) -> MatrixF64{
	let mut mat = MatrixF64::new(dim, dim).expect("Allocation failed");
	for i in 0..dim as i32 {
		set_periodic(&mut mat, i, i+1, -1.0/2.0/dx);
		set_periodic(&mut mat, i, i-1,  1.0/2.0/dx);
	}
	return mat;
}
// SI: B := -\mu F(x,t) + D\partial_x
pub fn construct_B_si(dim: usize, dx: f64, force: &Fn(f64, f64)-> f64, mu: f64, D: f64, t: f64) -> MatrixF64 {
	let mut mat = construct_periodic_derivation(dim, dx);
	mat.scale(D);
	for i in 0..dim as i32{
		assert!(get_periodic(&mat, i, i) == 0.0);
		set_periodic(&mut mat, i, i, -mu*force(i as f64*dx, t));
	}
	return mat;
}
