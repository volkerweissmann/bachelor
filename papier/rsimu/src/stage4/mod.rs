use ::*;

//Die zweite Zahl in jedem secs element (i32) ist die breite des Abschnitts, die erste (f64) ist die kraft mal die breite des Abschnitts. (Alles in natürlichen Einheiten)
fn anal_ness_sec_const_jump(secs: &Vec<(f64, usize)>) -> VectorF64 { //doesn't work if j is zero or at least on force is zero
	/*for f in secs.iter() {
		assert!(f.1 >= 0);
	}*/
	let lf = secs.len();
	let mut sum = 0.0;
	for o in 0..lf {
		let mut next = {
			if o != lf-1 {
				secs[o].1 as f64/secs[o].0 - secs[o+1].1 as f64/secs[o+1].0
			} else {
				secs[o].1 as f64/secs[o].0 - secs[0].1 as f64/secs[0].0
			}
		};
		for i in o+1..lf {
			next *= (secs[i].0).exp()
		}
		sum += next;
	}
	let mut prod1 = 1.0;
	for f in secs.iter() {
		prod1 *= (f.0).exp() // e^{beta*f*delta x}
	}
	let mut cs = Vec::new();
	cs.push(sum/(1.0-prod1));
	for i in 0..lf-1 {
		let csl = cs[i];
		cs.push( (secs[i].1 as f64/secs[i].0 - secs[i+1].1 as f64/secs[i+1].0)+(secs[i].0).exp()*csl); //c_{i+1} = d_i + f_i c_i
	}	
	let mut integral = 0.0; //=1/j
	for i in 0..lf {
		integral += secs[i].1 as f64*secs[i].1 as f64/secs[i].0 + cs[i]*secs[i].1 as f64/secs[i].0*((secs[i].0 as f64).exp()-1.0);
	}
	let mut dim = 0;
	for f in secs.iter() {
		dim += f.1;
	}
	let mut p = VectorF64::new(dim as usize).expect("allocation failed");
	let mut index = 0;
	for fi in 0..lf {
		let startindex = index;
		for _i in 0..secs[fi].1 {
			p.set(index, (cs[fi]*(secs[fi].0/secs[fi].1 as f64*(index-startindex)as f64).exp()+secs[fi].1 as f64/secs[fi].0)/integral);
			index += 1;
		}
	}
	return p;
}
pub fn compare_stages_3_4() {
	let dimfac: usize = 10000;
	let secs = vec![(1.1, dimfac), (0.1, dimfac), (-0.9, dimfac), (0.1, dimfac)];
	let pa = anal_ness_sec_const_jump(&secs);
	let mut forces = secs;
	for f in forces.iter_mut() {
		f.0 = f.0/f.1 as f64;
	}
	let pb = stage3::anal_ness_sec_const(&forces);
	//plotB(&pa, &pb);
	for i in 0..pa.len() {
		if (pa.get(i)-pb.get(i)).abs()*pa.len() as f64 > 0.001 {
			panic!("comparison of stage 3 and 4 failed");
		}
	}

	let dimfac: usize = 10000;
	let secs = vec![(1.1, 0), (0.1, dimfac), (-0.9, 0), (0.1, dimfac)];
	let pa = anal_ness_sec_const_jump(&secs);
	let forces = vec![(1.1, 1), (0.1/(dimfac-1) as f64, dimfac-1), (-0.9, 1), (0.1/(dimfac-1) as f64, dimfac-1)];
	let pb = stage3::anal_ness_sec_const(&forces);

	let mut failcount = 0;
	for i in 0..pa.len() {
		if (pa.get(i)-pb.get(i)).abs()*pa.len() as f64 > 0.001 {
			failcount += 1;
		}
	}
	if failcount > 2 {
		panic!("comparison of stage 3 and 4 failed");
	}
	println!("stages 3 and 4 match");
}