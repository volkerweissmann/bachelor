use ::*;

fn anal_wahlabend(V0: f64, D: f64, a: f64, t: f64, x: f64) -> f64 {
	let mut sum = 0.;
	for k in 0..100 {
		sum += (V0/2.).powi(k)*(
			 (a+x).signum()*(-1./2./t/D*((a+x).abs()+a*(1.+2.*k as f64)).powi(2)  ).exp()
			+(a-x).signum()*(-1./2./t/D*((a-x).abs()+a*(1.+2.*k as f64)).powi(2)  ).exp()
	        );
	}
	let pp = V0/4./(std::f64::consts::PI*D*t).sqrt() * sum;
	//return pp;
	return 1./(std::f64::consts::PI*4.*D*t).sqrt()*(-x*x/(4.*D*t)).exp()-pp;
	}
	fn anal_step(V0: f64, D: f64, a: f64, t: f64, x: f64) -> f64{
	let homo = (-(x).powi(2)/(4.*D*t)).exp();
	let val = {
		if x <= a {
			homo + (V0/2.).tanh()*(-(2.*a-x).powi(2)/(4.*D*t)).exp()
		} else {
			homo - (V0/2.).tanh()*(-(x).powi(2)/(4.*D*t)).exp()
		}
	};
	return val/(std::f64::consts::PI*4.*D*t).sqrt();
}
pub fn plotty() {
	let dim = 1000;
	let D = 0.2;
	let x0 = dim/2;
	let a = 50;
	let V0 = 1.;
	let mut data = vec![];
	for t in 0..10000 {
		let mut anal = anal_wahlabend(V0, D, a as f64, t as f64*1000000., (x0 as i32 - x0 as i32+1) as f64);
		if anal.is_nan() {
			anal = 0.;
		}
		data.push(anal);
	}
	plotC(&data);
}
pub fn animate() {
	//plotty();
	//return;
	/*println!("{}", anal_wahlabend(1., 0.2, 50., 10000., 0.));
	println!("{}", anal_wahlabend(1., 0.2, 50., 10000., 1.));
	return;*/
	let dim = 2000;
	let D = 0.2;
	let x0 = dim/2;
	let a = 50;
	let V0 = -1.;
	let pot = |x| {
		//return x as f64*0.1;
		/*if x > x0 - a && x < x0 + a {
			return V0;
	}*/
		if x > x0 + a {
			return V0;
		}
		return 0.;
	};
	
	let mut p = vec![0.; dim];
	p[x0] = 1.;	
	assert!(p.len() == dim);
	for &v in p.iter() {
		assert!(v >= 0.);
	}
	let mat = stage5::construct_timeder_operator_pot(dim, D, &pot);
	for i in 0..dim as i32 {
		for o in 2..dim as i32 -1 {
			assert!(get_periodic(&mat, i, i+o) == 0.);
		}
	}
	let mut closure = |_t: f64, y: &[f64], f: &mut [f64]| -> rgsl::GSLResult<()> {
		//let mut vec = VectorF64::from_slice(f).expect("gsl");
		//let mut vec = VectorView::from_array(f);
		//dgemv(NoTrans, 1., &mat, &VectorF64::from_slice(y).expect("gsl"), 0., &mut vec);
		for i in 0..dim {
			f[i] = mat.get(i,i)*y[i] + mat.get(per(dim, i as i32+1),i)*y[per(dim, i as i32+1)] + mat.get(per(dim, i as i32-1),i)*y[per(dim, i as i32-1)];
		}
		return rgsl::GSLResult::from(rgsl::Value::Success);
	};
	let mut sys = rgsl::types::ordinary_differential_equations::ODEiv2System::new(dim, &mut closure);
	let mut driver = rgsl::types::ordinary_differential_equations::ODEiv2Driver::alloc_y_new(&mut sys, &rgsl::types::ordinary_differential_equations::ODEiv2StepType::rk45(), 1e-6, 1e-6, 0.).expect("alloc failed");
	let mut t = 0.;
	//let phi = Vec::new();
	for i in 0..1+100 {
		if i != 0 {
			let ti = i as f64*100.;
			let start = std::time::Instant::now();
			driver.apply(&mut t, ti, &mut p).expect("apply failed");
			println!("{:?}", start.elapsed());
		}
		let mut dat = File::create(format!("out/{}.out", i)).expect("unable to create file");
		for i in 0..p.len() {
			let mut anal_v = anal_step(V0, D, a as f64, t, (i as i32 - x0 as i32) as f64);
			if anal_v.is_nan() {
				anal_v = 0.;
			}
			dat.write(format!("{}\t{}\n", p[i], anal_v).as_bytes()).expect("unable to write");
		}
		//let anal_o = anal_wahlabend(V0, D, a as f64, t, (a as i32 - x0 as i32+1) as f64);
		//let anal_i = anal_wahlabend(V0, D, a as f64, t, (a as i32 - x0 as i32-1) as f64);
		/*let mut max = 0.;
		let mut old = 0.;
		let mut maxi = 0;
		for i in 0..p.len() {
			let mut anal_v = anal_step(V0, D, a as f64, t, (i as i32 - x0 as i32) as f64);
			if anal_v.is_nan() {
				anal_v = 0.;
			}
			let d = anal_v-old;
			if d > max {
				max = d;
				maxi = i;
			}
			old = anal_v;
		}
		println!("{} {}", maxi, max);*/
		/*let homo = (-(a).powi(2)/(4.*D*t)).exp();
		let val = {
			homo + (V0/2.).tanh()*(-(2.*a-a).powi(2)/(4.*D*t)).exp()
		};*/
		let anal_o = anal_step(V0, D, a as f64, t, a as f64 + 0.001);
		let anal_i = anal_step(V0, D, a as f64, t, a as f64 - 0.001);
		println!("t={} num:{} anal:{}", t, p[x0+a+1]/p[x0+a-1], anal_o/anal_i);
	}
}