import sys
import os
import numpy as np
import matplotlib.pyplot as plt

pos = 300
def press(event):
	global pos
	sys.stdout.flush()
	oldpos = pos
	if event.key == "right" and os.path.exists("out/"+str(pos+1)+".out"):
		pos += 1
	elif event.key == "left" and os.path.exists("out/"+str(pos-1)+".out"):
		pos -= 1
	data  = {}
	matrix = np.loadtxt(fname="out/"+str(pos)+".out", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
		data[i+1] = matrix[:,i]
	line.set_xdata(data[1])
	line.set_ydata(data[5])
	#ax.set_ylim(np.min(dat), np.max(dat))
	ax.set_xlim(0, 800)
	ax.set_ylim(0, 0.01)
	ax.set_title(str(pos))
	fig.canvas.draw()
	
fig, ax = plt.subplots()
fig.canvas.mpl_connect('key_press_event', press)

line, = ax.plot([], [])
plt.show()

