#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)

times = [2000, 10000, 40000, 200000]
indices = [(0,0), (0,1), (1,0), (1,1)]

fig, axs = plt.subplots(2, 2, sharex="col", sharey="row", gridspec_kw={"hspace":0, "wspace":0})

#axs[0,0].text(500, 0.012, "$t=2000$")
#axs[0,1].text(500, 0.012, "$t=10000$")
#axs[1,0].text(500, 0.0038, "$t=40000$")
#axs[1,1].text(500, 0.0038, "$t=200000$")



xfac = 200

for subi in range(4):
	t = times[subi]

	axs[indices[subi][0],indices[subi][1]].annotate("$t="+str(round(t/xfac/xfac*0.2,5))+"$", xy=(0.95, 0.95), xycoords='axes fraction', fontsize=16,
                horizontalalignment='right', verticalalignment='top')
	
	data  = {}
	matrix = np.loadtxt(fname="singles/num_" + str(t) + ".dat", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
		data[i+1] = matrix[:,i]
	l1 = axs[indices[subi][0],indices[subi][1]].plot(data[0]/xfac,data[1]*xfac, linestyle="-", zorder=0, color="black", alpha=1) #zorder=100

	data  = {}
	matrix = np.loadtxt(fname="singles/anal_0_" + str(t) + ".dat", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
		data[i+1] = matrix[:,i]
	l2 = axs[indices[subi][0],indices[subi][1]].plot(data[0]/xfac,data[1]*xfac, linestyle="--", dashes=(1, 2, 0, 0), color=plt.cm.plasma(0.8), alpha=1)

	data  = {}
	matrix = np.loadtxt(fname="singles/anal_1_" + str(t) + ".dat", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
		data[i+1] = matrix[:,i]
	l3 = axs[indices[subi][0],indices[subi][1]].plot(data[0]/xfac,data[1]*xfac, linestyle="--", dashes=(1, 2, 0, 0), zorder=10, color=plt.cm.plasma(0.5), alpha=1)

	data  = {}
	matrix = np.loadtxt(fname="singles/anal_2_" + str(t) + ".dat", ndmin=2) #data["x"],data["y"],data["z"] = np.loadtxt(fname="data.dat", unpack=True)
	data[0] = np.array(list(range(0,len(matrix))))
	for i in range(0,len(matrix[0])):
		data[i+1] = matrix[:,i]
	l4 = axs[indices[subi][0],indices[subi][1]].plot(data[0]/xfac,data[1]*xfac, linestyle="--", dashes=(1, 2, 0, 1), zorder=20, color=plt.cm.plasma(0.3), alpha=1)

fig.legend([l1,l2,l3,l4], labels=["Numerisch","Ordnung 0","Ordnung 1","Ordnung 2"], loc="upper center", borderaxespad=0.1, title="Legende", ncol=4, framealpha=1)

#for ax in axs.flat:
axs[0,0].set(ylabel='p(x)')
axs[1,0].set(xlabel="x", ylabel='p(x)')
axs[1,1].set(xlabel="x")

#plt.xlabel("$x$", usetex=True)
#plt.ylabel("$p(x)$", usetex=True)
#plt.show()
plt.savefig("nof.pdf")

#plt.subplots_adjust(right=0.85)
#https://riptutorial.com/matplotlib/example/10473/single-legend-shared-across-multiple-subplots