set term pdf;
set output 'nof_3000.pdf';

set xlabel 'x';
set ylabel 'p(x)';

plot 'singles/num_3000.dat' using 0:1 t 'Numerisch' with lines, \
     'singles/anal_0_3000.dat' using 0:1 t 'Ordnung 0' with lines, \
     'singles/anal_1_3000.dat' using 0:1 t 'Ordnung 1' with lines, \
     'singles/anal_2_3000.dat' using 0:1 t 'Ordnung 2' with lines, \
     ;


set output 'nof_30000.pdf';

set xlabel 'x';
set ylabel 'p(x)';

plot 'singles/num_30000.dat' using 0:1 t 'Numerisch' with lines, \
     'singles/anal_0_30000.dat' using 0:1 t 'Ordnung 0' with lines, \
     'singles/anal_1_30000.dat' using 0:1 t 'Ordnung 1' with lines, \
     'singles/anal_2_30000.dat' using 0:1 t 'Ordnung 2' with lines, \
     ;


set output 'nof_200000.pdf';

set xlabel 'x';
set ylabel 'p(x)';

plot 'singles/num_200000.dat' using 0:1 t 'Numerisch' with lines, \
     'singles/anal_0_200000.dat' using 0:1 t 'Ordnung 0' with lines, \
     'singles/anal_1_200000.dat' using 0:1 t 'Ordnung 1' with lines, \
     'singles/anal_2_200000.dat' using 0:1 t 'Ordnung 2' with lines, \
     ;