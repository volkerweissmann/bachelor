#!/usr/bin/python3
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)


#Ableitungsregeln berechnen
points = 1
matrix = np.zeros((points,points))
for i in range(0,points):
    for j in range(0,points):
        matrix[i,j] = (j+1)**(2*i+1)/math.factorial(2*i+1)
target = np.zeros(points)
target[0] = 1
#print(matrix)
der = np.linalg.solve(matrix, target)
#print(der)



dim = 1000
xnew1 = 0
xnew2 = 0.4*dim
xnew3 = 0.6*dim
xnew4 = 0.9*dim
xnew5 = 1*dim

dx1 = xnew2-xnew1
dx2 = xnew3-xnew2
dx3 = xnew4-xnew3
dx4 = xnew5-xnew4

force1 = 1/dim
force2 = -3/dim
force3 = 2/dim
force4 = 3/dim

mat = np.zeros((dim, dim))

#j = mu F(x)p(x) - D dp(x)/dx
#j/D = mu/D F(x) p(x) - dp(x)/dx

for i in range(0, dim):
    if i < xnew2:
        f = force1
    elif i < xnew3:
        f = force2
    elif i < xnew4:
        f = force3
    else:
        f = force4
    mat[i,i] = f
    # p'(x) = (p(x+dx)-p(x-dx))/(2*dx)
    for k in range(0,points):
        index = i+k+1
        if index >= dim:
            index -= dim
        mat[i,index] = -der[k]/(2.0)
        index = i-k-1
        mat[i,index] = der[k]/(2.0)

print("calling numpy.linalg.solve...")
p = np.linalg.solve(mat, np.ones(dim))
print("...finished numpy.linalg.solve")
integral = sum(p) # =D/j
p /= integral



#xar1 = np.arange(0,x1,xstep)
#xar2 = np.arange(x1,x1+x2,xstep)
#xar3 = np.arange(x1+x2,x1+x2+x3,xstep)
#xar4 = np.arange(x1+x2+x3,x1+x2+x3+x4,xstep)
xar1 = np.arange(xnew1,xnew2)
xar2 = np.arange(xnew2,xnew3)
xar3 = np.arange(xnew3,xnew4)
xar4 = np.arange(xnew4,xnew5)
k = -(1/force4 - 1/force1 + math.exp(force4*dx4)*(
    1/force3 - 1/force4 + math.exp(force3*dx3)*(
    1/force2 - 1/force3 + math.exp(force2*dx2)*(
    1/force1 - 1/force2))))
h1 = k/(math.exp(dx1*force1+dx2*force2+dx3*force3+dx4*force4)-1)
h2 = h1*math.exp(dx1*force1)+1/force1-1/force2
h3 = h2*math.exp(dx2*force2)+1/force2-1/force3
h4 = h3*math.exp(dx3*force3)+1/force3-1/force4

if abs(1-(h4*math.exp(dx4*force4)+1/force4-1/force1)/h1) > 0.01:
    print("bad solve")


yar1 = h1*np.exp(force1*(xar1-xnew1*np.ones( len(xar1)   ))) + 1/force1
yar2 = h2*np.exp(force2*(xar2-xnew2*np.ones( len(xar2)   ))) + 1/force2
yar3 = h3*np.exp(force3*(xar3-xnew3*np.ones( len(xar3)   ))) + 1/force3
yar4 = h4*np.exp(force4*(xar4-xnew4*np.ones( len(xar4)   ))) + 1/force4


N = dx1/force1 + h1/force1*(math.exp(dx1*force1)-1) \
  + dx2/force2 + h2/force2*(math.exp(dx2*force2)-1) \
  + dx3/force3 + h3/force3*(math.exp(dx3*force3)-1) \
  + dx4/force4 + h4/force4*(math.exp(dx4*force4)-1)


yar1 /= N
yar2 /= N
yar3 /= N
yar4 /= N

#dat = open("stat.dat", "w")
#for i in range(dim):
#	dat.write(str(i*xstep) + " " + str(p[i]) + " ")
#	if i < len(yar1):
#		dat.write(str(yar1[i]))
#	elif i < len(yar1)+len(yar2):
#		dat.write(str(yar2[i-len(yar1)]))
#	elif i < len(yar1)+len(yar2)+len(yar3):
#		dat.write(str(yar3[i-len(yar1)-len(yar2)]))
#	else:
#		dat.write(str(yar4[i-len(yar1)-len(yar2)-len(yar3)]))
#	dat.write("\n")
#plt.plot(xar1,yar1)
#plt.plot(xar2,yar2)
#plt.plot(xar3,yar3)
#plt.plot(xar4,yar4)
#plt.plot(np.concatenate((xar1,xar2,xar3,xar4)), np.concatenate((yar1,yar2,yar3,yar4)) , linestyle="--", zorder=100, label="Analytisch")
#plt.plot(np.arange(0,x1+x2+x3+x4, xstep),p, label="Numerisch")

plt.plot(np.arange(0,dim,1), np.concatenate((yar1,yar2,yar3,yar4)) , linestyle="--", zorder=100, label="Analytisch", alpha=1)
plt.plot(np.arange(0,dim, 1),p, label="Numerisch", alpha=1)
#plt.plot(np.arange(0,dim, 1),pdyn)
plt.legend(framealpha=1)
plt.xlabel("x")
plt.ylabel("p(x)")
#plt.show()
plt.savefig("stat.pdf")
