#!/usr/bin/python3

from math import *

print(r"\newcommand{\carc}[3]{++(#2:#1) arc (#2:#3:#1);}")
def ring(radius, gap):
	alpha = asin(gap/2/radius)
	lang = -alpha*180/pi + 90
	rang = alpha*180/pi  - 270
	src = "\\draw (0,0)\\carc{" + str(radius) + "}{" + str(lang) + "}{" + str(rang) + "}"
	print(src)
def connection(r1, r2, gap):
	x1 = sqrt(r1**2 - gap**2/4)
	x2 = sqrt(r2**2 - gap**2/4)
	src = "\\draw ({2},{0}) .. controls (-{2},{0}) and ({2},{1}) .. (-{2},{1});".format(x1,x2,gap/2)
	print(src)

ggap = 0.5
rads = [2,3,4]
ring(rads[0], ggap)
for i in range(1,len(rads)):
	ring(rads[i], ggap)
	connection(rads[i-1],rads[i],ggap)


radius = 3
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.2,-0.17) node {{$x_1$}};".format(90+3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (0.21,-0.17) node {{$x_4$}};".format(90-3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.2,0.2) node {{$x_2$}};".format(270-30, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (+0.2,0.2) node {{$x_3$}};".format(270+30, radius-0.3, radius+0.3))

radius = 4
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.2,-0.17) node {{$x_4$}};".format(90+3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (0.51,-0.17) node {{$x_4+p$}};".format(90-3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.5,0.2) node {{$x_2+p$}};".format(270-30, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (+0.45,0.2) node {{$x_3+p$}};".format(270+30, radius-0.3, radius+0.3))

radius = 2
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.5,-0.17) node {{$x_1-p$}};".format(90+3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (0.2,-0.17) node {{$x_1$}};".format(90-3, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.5,0.2) node {{$x_2-p$}};".format(270-30, radius-0.3, radius+0.3))
print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (+0.45,0.2) node {{$x_3-p$}};".format(270+30, radius-0.3, radius+0.3))