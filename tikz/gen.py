#!/usr/bin/python3

from math import *

def ring(radius, gap):
	alpha = asin(gap/2/radius)
	lang = -alpha*180/pi #+ 90
	rang = alpha*180/pi  - 360
	src = "\\draw (0,0)\\carc{" + str(radius) + "}{" + str(lang) + "}{" + str(rang) + "}"
	print(src)
def connection(r1, r2, gap):
	x1 = sqrt(r1**2 - gap**2/4)
	x2 = sqrt(r2**2 - gap**2/4)
	src = "\\draw ({0},{2}) .. controls ({0},-{2}) and ({1},{2}) .. ({1},-{2});".format(x1,x2,gap/2)
	print(src)

ggap = 0.5
rads = [3,4,5]
ring(rads[0], ggap)
for i in range(1,len(rads)):
	ring(rads[i], ggap)
	connection(rads[i-1],rads[i],ggap)


for radius in range(3,6):
	angle = 50
	print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (0,0.2) node {{$x=x_2$}};".format(angle, radius-0.3, radius+0.3))

	angle = 150
	print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.1,0.2) node {{$x=x_2$}};".format(angle, radius-0.3, radius+0.3))


	angle = 280
	print("\\draw (0,0) ++ ({0}:{1}) -- ({0}:{2}) ++ (-0.55,0) node {{$x=x_2$}};".format(angle, radius-0.3, radius+0.3))