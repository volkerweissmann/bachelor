#!/usr/bin/env python3

import numpy as np
from sympy import *
from sympy.assumptions.refine import refine_Relational
from collections import defaultdict

print("imported")

def ind(var, length):
	s = str(var)
	return " "*(length-len(s)) + s
def mid(var, length):
	s = str(var)
	space = length-len(s)
	left = space//2
	right = space-left
	return " "*left + s + " "*right
def trysubs(obj, subsdict):
	if isinstance(obj, int):
		return obj
	else:
		return obj.subs(subsdict)
def merge(a,b):
	ret = WriteOnceDict()
	for z0 in a:
		ret[z0] = a[z0]
	for z0 in b:
		if z0 not in ret:
			ret[z0] = b[z0]
		else:
			assert(b[z0]==ret[z0])
	return ret

V0 = symbols("V0")
c = symbols("c")
F = symbols("F")
xb = symbols("xb")
z = symbols("z")
x = symbols("x")
t = symbols("t")
r = symbols("r")
xp = symbols("xp")
c = symbols("c")
def to_list(x):
	if isinstance(x, int):
		assert(x==0)
		return 0, 0
	elif x == xp:
		return 1,0
	elif x == -xp:
		return -1,0
	elif x == r:
		return 0,1
	elif x == -r:
		return 0,-1
	else:
		print(x)
		print(x.args)
		print(type(x.args[0]))
		print(type(x.args[1]))
		raise TypeError("unknown case " + str(type(x)))
def is_a_leq_b(a,b):
	if a.rf == b.rf:
		return a.xpf <= b.xpf
	if a.rf <= b.rf and a.rf + a.xpf <= b.rf + b.xpf:
		return True
	if a.rf > b.rf and a.rf + a.xpf > b.rf + b.xpf:
		return False
	raise ValueError("unknown case")
def is_a_eq_b(a,b):
	assert(isinstance(a, Pos))
	assert(isinstance(b, Pos))
	return a.rf == b.rf and a.xpf == b.xpf
def is_prot(ar, z0):
	for a in ar:
		if is_a_leq_b(a[0], z0) and is_a_leq_b(z0, a[1]):
			return True
	return False
class Pos:
	rf = None
	xpf = None
	def __hash__(self):
		return hash((self.rf,self.xpf))
	def __init__(self, rf, xpf):
		self.rf = rf
		self.xpf = xpf
	def __str__(self):
		return " "*(2-len(str(self.rf))) + str(self.rf) + "*r + " + " "*(2-len(str(self.xpf))) + str(self.xpf) + "*xp"
	def __repr__(self):
		return str(self)
	def __eq__(self, other):
		return self.rf == other.rf and self.xpf == other.xpf
	def __le__(self, other):
		return is_a_leq_b(self, other)
	def __ge__(self, other):
		return is_a_leq_b(other, self)
	def __neg__(self):
		return Pos(-self.rf, -self.xpf)
	def __sub__(self, other):
		return Pos(self.rf-other.rf, self.xpf-other.xpf)
	def __add__(self, other):
		return Pos(self.rf+other.rf, self.xpf+other.xpf)

gprot = []
hprot = []
gdar = []
hdar = []

def gapp(deltalike):
	if is_prot(gdar, deltalike[0]):
			raise ValueError("Dont touch")
	gdar.append(deltalike)
def happ(deltalike):
	assert(not is_prot(hdar, deltalike[0]))
	hdar.append(deltalike)
def gset(deltalike):
	global gdar
	gdar = [x for x in gdar if not is_a_eq_b(x[0], deltalike[0])]
	gapp((deltalike[0], deltalike[1]))
	#gapp((deltalike[0], deltalike[1]-getat(gdar, deltalike[0])))
def hset(deltalike):
	global hdar
	hdar = [x for x in hdar if not is_a_eq_b(x[0], deltalike[0])]
	happ((deltalike[0], deltalike[1]))

def getat(ar, z0):
	ret = 0
	for dl in ar:
		if is_a_eq_b(z0, dl[0]):
			ret += dl[1]
	return ret
def tryfixhoch():
	#gf = tofunc(gdar)
	#hf = tofunc(hdar)
	#for dl in gdar + hdar:
	#	z0 = dl[0]
	#	res = trysubs(gf, {z: z0}) + trysubs(gf, {z: -z0}) - exp(V0)*trysubs(hf, {z: z0}) - exp(V0)*trysubs(hf, {z: -z0})
	#	print(res)
	z0s = []
	for dl in gdar + hdar:
		z0s.append(dl[0])
	for hp in hprot:
		for gp in gprot:
			if hp[0] == -gp[1] and hp[1] == -gp[0]:
				changeflag = False
				for z0 in z0s:
					if hp[0] <= z0 and z0 <= hp[1] and not is_prot(gprot, z0) and not is_prot(hprot, -z0):
						assert(gp[0] <= -z0 and -z0 <= gp[1])
						temp1 = 2*c/(1+c)    *getat(hdar,z0) + (c-1)/(c+1) *getat(gdar,-z0)
						temp2 = -(c-1)/(c+1) *getat(hdar,z0) + 2/(c+1)     *getat(gdar,-z0)
						gset(( z0,temp1 ))
						hset((-z0,temp2 ))
						changeflag = True
				if changeflag:
					hprot.append((-hp[1], -hp[0]))
					gprot.append((-gp[1], -gp[0]))
					return False
	# for z0 in z0s:
	# 	if is_prot(gprot, -z0) and is_prot(hprot, z0) and not is_prot(gprot, z0) and not is_prot(hprot, -z0):
	# 		print("test")
	# 		temp1 = 2*c/(1+c)    *getat(hdar,z0) + (c-1)/(c+1) *getat(gdar,-z0)
	# 		temp2 = -(c-1)/(c+1) *getat(hdar,z0) + 2/(c+1)     *getat(gdar,-z0)
	# 		gset(( z0,temp1 ))
	# 		hset((-z0,temp2 ))
	# 	elif is_prot(gprot, z0) and is_prot(hprot, -z0) and not is_prot(gprot, -z0) and not is_prot(hprot, z0):
	# 		temp1 = 2*c/(1+c)    *getat(hdar,-z0) + (c-1)/(c+1) *getat(gdar,z0)
	# 		temp2 = -(c-1)/(c+1) *getat(hdar,-z0) + 2/(c+1)     *getat(gdar,z0)
	# 		gset((-z0,temp1 ))
	# 		hset(( z0,temp2 ))
	# 	else:
	# 		raise ValueError("unknown case")
	for z0 in z0s:
		resp = getat(gdar, z0)+getat(gdar, -z0)-c*getat(hdar, z0)-c*getat(hdar, -z0) #c=exp(V)
		resj = getat(gdar, z0)-getat(gdar, -z0)-getat(hdar, z0)+getat(hdar, -z0)
		assert(0==simplify(resp*(c+1)))
		assert(0==simplify(resj))
	return True
def tryfixrunter():
	#gf = tofunc(gdar)
	#hf = tofunc(hdar)
	#for dl in gdar + hdar:
	#	z0 = dl[0]
	#	res = trysubs(gf, {z: z0}) + trysubs(gf, {z: -z0}) - exp(V0)*trysubs(hf, {z: z0}) - exp(V0)*trysubs(hf, {z: -z0})
	#	print(res)
	z0s = []
	for dl in gdar:
		z0s.append(dl[0])
	for dl in hdar:
		z0s.append(dl[0])
	for z0 in z0s:
		if is_prot(gprot, -z0) and is_prot(hprot, z0) and not is_prot(gprot, z0) and not is_prot(hprot, -z0):
			temp1 = 2/c/(1+1/c)    *exp(4*F*r)*getat(hdar,z0) + (1/c-1)/(1/c+1) *getat(gdar,-z0)
			temp2 = -(1/c-1)/(1/c+1) *exp(4*F*r)*getat(hdar,z0) + 2/(1/c+1)     *getat(gdar,-z0)
			gset(( z0,temp1 ))
			hset((-z0,temp2*exp(-4*F*r) ))
		elif is_prot(gprot, z0) and is_prot(hprot, -z0) and not is_prot(gprot, -z0) and not is_prot(hprot, z0):
			temp1 = 2/c/(1+1/c)    *exp(4*F*r)*getat(hdar,-z0) + (1/c-1)/(1/c+1) *getat(gdar,z0)
			temp2 = -(1/c-1)/(1/c+1) *exp(4*F*r)*getat(hdar,-z0) + 2/(1/c+1)     *getat(gdar,z0)
			gset((-z0,temp1 ))
			hset(( z0,temp2*exp(-4*F*r) ))
		else:
			raise ValueError("unknown case")
	for z0 in z0s:
		resp = getat(gdar, z0)+getat(gdar, -z0)-1/c*exp(4*F*r)*getat(hdar, z0)-1/c*exp(4*F*r)*getat(hdar, -z0) #c=exp(V)
		resj = getat(gdar, z0)-getat(gdar, -z0)-exp(4*F*r)*getat(hdar, z0)+exp(4*F*r)*getat(hdar, -z0)
		assert(0==simplify(resp*(1+1/c)))
		assert(0==simplify(resj))

# happ(( Pos(0,1), 1 ))
# gprot.append(( Pos(-1,0), Pos(0,0)))
# hprot.append((Pos(0,0),Pos(1,0)))
# print(tryfixhoch())
# print(tryfixhoch())

class WriteOnceDict(dict):
    def __setitem__(self, key, value):
        if key in self:
            raise KeyError('{} has already been set'.format(key))
        super(WriteOnceDict, self).__setitem__(key, value)

def getz0s(sgdir, shdir):
	z0s = []
	for z0 in sgdir:
		if not z0 in z0s:
			z0s.append(z0)
	for z0 in shdir:
		if not z0 in z0s:
			z0s.append(z0)
	return z0s
USE_ABBREV = True
f,t,k,j = symbols("f t k j")
def fixhoch(gdirarg, hdirarg, checks=True):
	gdir = mult_and_shift(gdirarg, 1, Pos(0,0))
	hdir = mult_and_shift(hdirarg, 1, Pos(0,0))
	z0s = getz0s(gdir, hdir)
	for z0 in z0s:
		if -z0 in gdir and z0 in hdir and z0 in gdir and -z0 in hdir:
			pass
		elif -z0 in gdir and z0 in hdir and not z0 in gdir and not -z0 in hdir:
			if USE_ABBREV:
				temp1 = j    *hdir[z0] + t *gdir[-z0]
				temp2 = -t *hdir[z0] + k     *gdir[-z0]
			else:
				temp1 = 2*c/(1+c)    *hdir[z0] + (c-1)/(c+1) *gdir[-z0]
				temp2 = -(c-1)/(c+1) *hdir[z0] + 2/(c+1)     *gdir[-z0]
			gdir[z0] = temp1
			hdir[-z0] = temp2
		elif z0 in gdir and -z0 in hdir and not -z0 in gdir and not z0 in hdir:
			if USE_ABBREV:
				temp1 = j    *hdir[-z0] + t *gdir[z0]
				temp2 = -t *hdir[-z0] + k     *gdir[z0]
			else:
				temp1 = 2*c/(1+c)    *hdir[-z0] + (c-1)/(c+1) *gdir[z0]
				temp2 = -(c-1)/(c+1) *hdir[-z0] + 2/(c+1)     *gdir[z0]
			gdir[-z0] = temp1
			hdir[z0] = temp2
		else:
			raise ValueError("unknown case")
	if checks:
		z0s = getz0s(gdir, hdir)	
		eV = c
		for z0 in z0s:
			resp = gdir[z0]+gdir[-z0]-eV*hdir[z0]-eV*hdir[-z0]
			resj = gdir[z0]-gdir[-z0]-hdir[z0]+hdir[-z0]
			assert(0==simplify(resp*(eV+1)))
			assert(0==simplify(resj))
	return gdir,hdir
def mult_and_shift(ar, mult, shift):
	ret = {}
	for pos in ar:
		ret[pos-shift] = ar[pos]*mult
	return ret
def fixrunter(gdir, hdir, checks=True):
	gcur = mult_and_shift(gdir, 1, Pos(-1,0))
	if USE_ABBREV:
		hcur = mult_and_shift(hdir, f, Pos(1,0))
	else:
		hcur = mult_and_shift(hdir, exp(4*F*r), Pos(1,0))
	z0s = getz0s(gcur, hcur)
	for z0 in z0s:
		if -z0 in gcur and z0 in hcur and z0 in gcur and -z0 in hcur:
			pass
		elif -z0 in gcur and z0 in hcur and not z0 in gcur and not -z0 in hcur:
			assert(False)
			if USE_ABBREV:
				temp1 = k     *hcur[z0] - t *gcur[-z0]
				temp2 = t *hcur[z0] + j   *gcur[-z0]
			else:
				temp1 = 2/(1+c)     *hcur[z0] - (c-1)/(c+1) *gcur[-z0]
				temp2 = (c-1)/(c+1) *hcur[z0] + 2*c/(c+1)   *gcur[-z0]
			gcur[z0] = temp1
			hcur[-z0] = temp2
		elif z0 in gcur and -z0 in hcur and not -z0 in gcur and not z0 in hcur:
			if USE_ABBREV:
				temp1 = k     *hcur[-z0] - t   *gcur[z0]
				temp2 = t *hcur[-z0] + j     *gcur[z0]
			else:
				temp1 = 2/(1+c)     *hcur[-z0] - (c-1)/(c+1)   *gcur[z0]
				temp2 = (c-1)/(c+1) *hcur[-z0] + 2*c/(c+1)     *gcur[z0]
			gcur[-z0] = temp1
			hcur[z0] = temp2
		else:
			raise ValueError("unknown case")
	gdir = mult_and_shift(gcur, 1, Pos(1,0))
	if USE_ABBREV:
		hdir = mult_and_shift(hcur, 1/f, Pos(-1,0))
	else:
		hdir = mult_and_shift(hcur, exp(-4*F*r), Pos(-1,0))
	if checks:
		z0s = getz0s(gcur, hcur)	
		eV = 1/c
		for z0 in z0s:
			resp = gcur[z0]+gcur[-z0]-eV*hcur[z0]-eV*hcur[-z0]
			resj = gcur[z0]-gcur[-z0]-hcur[z0]+hcur[-z0]
			assert(0==simplify(resp*(eV+1)))
			assert(0==simplify(resj))
		checkrunter(gdir,hdir)
	return gdir,hdir
def checkrunter(gdir,hdir):
	gcur = mult_and_shift(gdir, 1, Pos(-1,0))
	if USE_ABBREV:
		hcur = mult_and_shift(hdir, f, Pos(1,0))
	else:
		hcur = mult_and_shift(hdir, exp(4*F*r), Pos(1,0))
	z0s = getz0s(gcur, hcur)
	eV = 1/c
	for z0 in z0s:
		if USE_ABBREV:
			resp = gdir[z0-Pos(1,0)]+gdir[-z0-Pos(1,0)]-f*eV*hdir[z0+Pos(1,0)]-f*eV*hdir[-z0+Pos(1,0)]
			resj = gdir[z0-Pos(1,0)]-gdir[-z0-Pos(1,0)]-f*hdir[z0+Pos(1,0)]+f*hdir[-z0+Pos(1,0)]
		else:
			resp = gdir[z0-Pos(1,0)]+gdir[-z0-Pos(1,0)]-exp(4*F*r)*eV*hdir[z0+Pos(1,0)]-exp(4*F*r)*eV*hdir[-z0+Pos(1,0)]
			resj = gdir[z0-Pos(1,0)]-gdir[-z0-Pos(1,0)]-exp(4*F*r)*hdir[z0+Pos(1,0)]+exp(4*F*r)*hdir[-z0+Pos(1,0)]
		assert(0==simplify(resp*(eV+1)))
		assert(0==simplify(resj))
def check(gdir,hdir):
	z0s = getz0s(gdir, hdir)	
	eV = c
	for z0 in z0s:
		if abs(z0.rf) < 7:
			resp = gdir[z0]+gdir[-z0]-eV*hdir[z0]-eV*hdir[-z0]
			resj = gdir[z0]-gdir[-z0]-hdir[z0]+hdir[-z0]
			assert(0==simplify(resp*(eV+1)))
			assert(0==simplify(resj))
	gcur = mult_and_shift(gdir, 1, Pos(-1,0))
	if USE_ABBREV:
		hcur = mult_and_shift(hdir, f, Pos(1,0))
	else:
		hcur = mult_and_shift(hdir, exp(4*F*r), Pos(1,0))
	z0s = getz0s(gcur, hcur)
	eV = 1/c
	for z0 in z0s:
		if abs(z0.rf) < 7:
			resp = gcur[z0]+gcur[-z0]-eV*hcur[z0]-eV*hcur[-z0]
			resj = gcur[z0]-gcur[-z0]-hcur[z0]+hcur[-z0]
			assert(0==simplify(resp*(eV+1)))
			assert(0==simplify(resj))
			
	gcur = mult_and_shift(gdir, 1, Pos(-1,0))
	if USE_ABBREV:
		hcur = mult_and_shift(hdir, f, Pos(1,0))
	else:
		hcur = mult_and_shift(hdir, exp(4*F*r), Pos(1,0))
	z0s = getz0s(gcur, hcur)
	eV = 1/c
	for z0 in z0s:
		if abs(z0.rf) < 7:
			if USE_ABBREV:
				resp = gdir[z0-Pos(1,0)]+gdir[-z0-Pos(1,0)]-f*eV*hdir[z0+Pos(1,0)]-f*eV*hdir[-z0+Pos(1,0)]
				resj = gdir[z0-Pos(1,0)]-gdir[-z0-Pos(1,0)]-f*hdir[z0+Pos(1,0)]+f*hdir[-z0+Pos(1,0)]
			else:
				resp = gdir[z0-Pos(1,0)]+gdir[-z0-Pos(1,0)]-exp(4*F*r)*eV*hdir[z0+Pos(1,0)]-exp(4*F*r)*eV*hdir[-z0+Pos(1,0)]
				resj = gdir[z0-Pos(1,0)]-gdir[-z0-Pos(1,0)]-exp(4*F*r)*hdir[z0+Pos(1,0)]+exp(4*F*r)*hdir[-z0+Pos(1,0)]
			assert(0==simplify(resp*(eV+1)))
			assert(0==simplify(resj))

GDIR = WriteOnceDict()
HDIR = WriteOnceDict()
sg = 0
sh = 1

sar = np.array([sg,sh])

#R = np.array([[-t, k*f], [j/f, t]])
#H = np.array([[t, j], [k, -t]])
R = np.array([[-(c-1)/(c+1), 2/(c+1)*f], [2*c/(c+1)/f, (c-1)/(c+1)]])
H = np.array([[(c-1)/(c+1), 2*c/(c+1)], [2/(c+1), -(c-1)/(c+1)]])
for n in range(0, 4):
	print(n)
	GDIR[Pos(-2*n,-1)] = simplify(np.linalg.matrix_power(R.dot(H), n).dot(sar)[0])
	GDIR[Pos(2*n,1)] = simplify(H.dot(np.linalg.matrix_power(R.dot(H), n)).dot(sar)[0])
	if n != 0:
		GDIR[Pos(-2*n,1)] = simplify(np.linalg.matrix_power(R.dot(H), n).dot(H.dot(sar))[0])
		GDIR[Pos(2*n,-1)] = simplify(H.dot(np.linalg.matrix_power(R.dot(H), n)).dot(H.dot(sar))[0])
	HDIR[Pos(2*n,1)] = simplify(np.linalg.matrix_power(R.dot(H), n).dot(sar)[1])
	HDIR[Pos(-2*n,-1)] = simplify(H.dot(np.linalg.matrix_power(R.dot(H), n)).dot(sar)[1])
	if n != 0:
		HDIR[Pos(2*n,-1)] = simplify(np.linalg.matrix_power(R.dot(H), n).dot(H.dot(sar))[1])
		HDIR[Pos(-2*n,1)] = simplify(H.dot(np.linalg.matrix_power(R.dot(H), n)).dot(H.dot(sar))[1])
print("finished building, starting checking")
check(GDIR,HDIR)
		
exit(0)
GDIR[Pos(0,-1)] = sg
HDIR[Pos(0,1)] = sh
for i in range(0,5):
	print("round", i)
	GDIR,HDIR = fixhoch(GDIR,HDIR,checks=False)
	GDIR,HDIR = fixrunter(GDIR,HDIR,checks=False)

z0s = []
for rf in range(-6,7):
	z0s.append(Pos(rf,-1))
	#z0s.append(Pos(rf,0))
	z0s.append(Pos(rf,1))
gdef = {}
for z0 in z0s:
	if z0 in GDIR:
		gdef[z0] = simplify(GDIR[z0])
	else:
		gdef[z0] = symbols("u")
hdef = {}
for z0 in z0s:
	if z0 in HDIR:
		hdef[z0] = simplify(HDIR[z0])
	else:
		hdef[z0] = symbols("u")


exit(0)
A = np.array([[k*k*f-t*t, -k*t*f-t*j],[k*t+t*j/f, -t*t+j*j/f]])
vec = np.array([startw,startz])
for n in range(0, 4):
	print(simplify( gdef[Pos(-2*n,-1)] -vec[0]  ))
	print(simplify( hdef[Pos(2*n,1)] -vec[1]  ))
	vec = A.dot(vec)

exit(0)
for rf in range(-4,5,2):
	print(mid(str(rf)+"-",40), end="")
	print(mid(gdef[Pos(rf,-1)],40), end="|")
	print(mid(hdef[Pos(rf,-1)],40))

	print(mid(str(rf)+"+",40), end="")
	print(mid(gdef[Pos(rf,+1)],40), end="|")
	print(mid(hdef[Pos(rf,+1)],40))
exit(0)
for rf in range(-2,3):
	print(mid(str(rf)+"-",10), end="")
	print(mid(str(rf)+"+",10), end="")
print("")
for rf in range(-2,3):
	print(mid(gdef[Pos(rf,-1)],10), end="")
	print(mid(gdef[Pos(rf,1)],10), end="")
print("")
for rf in range(-2,3):
	print(mid(hdef[Pos(rf,-1)],10), end="")
	print(mid(hdef[Pos(rf,1)],10), end="")	
print("")
exit(0)
# z0s = getz0s(gdir, {})
# for z0 in z0s:
# 	print(z0)
# exit(0)
print("g-xb, g+xb, h-xh, h+xb")
for rf in range(-4,5):
	print(ind(rf,2), simplify(gdef[Pos(rf, -1)]), "|||", simplify(gdef[Pos(rf, 1)]), "|||", simplify(hdef[Pos(rf, -1)]), "|||", simplify(hdef[Pos(rf, 1)]))
exit(0)
z0s = getz0s(gdir, hdir)
for i in range(0,1):
	fixhoch(checks=False)
	fixrunter(checks=False)
z0new = getz0s(gdir, hdir)

for z0 in z0new:
	if z0 not in z0s:
		print(z0)