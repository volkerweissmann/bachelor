#!/usr/bin/env sage

from sage.all import *

import time
startTimeMillis = int(round(time.time() * 1000))
##Returns milliseconds since program startup
def millis():
	return int(round(time.time() * 1000))-startTimeMillis

import sys
sys.path.append("/home/volker/git/latex2sympy")
import latex2sympy
import sympy

x = var("x")
z = var("z")

def str2sym(texin):
	texin = texin.replace(r"\\&"," ").replace(r"\lou", r"\left(").replace(r"\rou", r"\right)")
	return latex2sympy.latex2sympy(texin, latex2sympy.ConfigL2S()).subs("\\pi", pi).subs("\\beta", "beta")
	

pl = str2sym(r"\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou + \frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x-a-F_1t_1)^2}{t_1}\rou e^{4Fa}\tanh\frac{\beta V_0}2 \\&+F_1\tanh\frac{\beta V_0}2\lou1+\tanh\frac{\beta V_0}2\rou\exp\lou F_1^2\lou\lou\tanh\frac{\beta V_0}2\rou^2-1\rou t_1  + 2F_1\lou1-\tanh\frac{\beta V_0}2\rou a + 2F_1\lou1+\tanh\frac{\beta V_0}2\rou x \rou   \lou 1-\erf\lou\frac{a-x-t_1 F\tanh\frac{\beta V_0}2}{\sqrt{t_1}}\rou\rou")
pr = str2sym(r"\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou\lou 1+ \tanh\frac{\beta V_0}2\rou\\&+e^{-\beta V_0}F_1\tanh\frac{\beta V_0}2\lou1+\tanh\frac{\beta V_0}2\rou\exp\lou F_1^2\lou\lou\tanh\frac{\beta V_0}2\rou^2-1\rou t_1  + 2F_1\lou1-\tanh\frac{\beta V_0}2\rou a + 2F_1\lou1-\tanh\frac{\beta V_0}2\rou x \rou   \lou 1-\erf\lou\frac{a+x-t_1 F\tanh\frac{\beta V_0}2}{\sqrt{t_1}}\rou\rou")

left = simplify(diff(pl, x)(x=0))
right = simplify(diff(pr, x)(x=0))
print(latex(simplify(left/right)))


exit(0)

#texin = r"\frac 1{\sqrt{t_1}}\exp\lou-\frac{(x-z-F_1t_1)^2}{t_1}+d_2z\rou d_1"
#texin = r"\frac 1{\sqrt{t_1}}\exp\lou-\frac{(x+z-F_1t_1)^2}{t_1}+z(d_2-4F_1)\rou d_1"
#texin = r"\frac 1{\sqrt{t_1}}\exp\lou-\frac{(x-x_0-F_1t_1)^2}{t_1}\rou + \frac 1{\sqrt{t_1}}\exp\lou-\frac{(x-|x_0|-F_1t_1)^2}{t_1}\rou d_0"
#texin = r"\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou"
#texin = r"\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou + s_l\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x-a-F_1t_1)^2}{t_1}\rou\\&+c\exp\lou 2bx - 2F_1 bt_1 + b^2 t_1\rou\lou\erf\lou\frac{a - x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"


#return integrate(sym, var("x"), -infinity, 0)



assume(var("t_1") > 0)
assume(var("a") > 0)
assume(var("b") > 0)

left = integrate(str2sym(r"\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou + s_l\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x-a-F_1t_1)^2}{t_1}\rou\\&+c\exp\lou 2bx - 2F_1 bt_1 + b^2 t_1\rou\lou\erf\lou\frac{a - x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), -infinity, 0)
right1 = integrate(str2sym(r"e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou"), var("x"), 0, infinity)
assume(var("b")-2*var("F_1") > 0)
right2 = integrate(str2sym(r"e^{-\beta V_0}c\exp\lou -2bx-2F_1bt_1 + b^2t_1 + 4F_1x \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)
print(latex(simplify(left+right1+right2)))
#print(latex(simplify(-(left+right1+right2)*var("b")*2)))


#e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou
#\\&+



#assume(var("d") < 0)

#right = integrate(str2sym(r"\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou")+str2sym(r"c\exp\lou x-bt_1  \rou(\erf(x)-1)"), var("x"), 0, infinity)


#right = integrate(str2sym(r"\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou\\&+c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)

#right = integrate(str2sym(r"\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou+c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)





#right = integrate(str2sym(r"e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou"), var("x"), 0, infinity)
#right = integrate(str2sym(r"e^{-\beta V_0}c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)



#right = integrate(str2sym(r"e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou\\&+e^{-\beta V_0}c\exp\lou -2bx-2F_1bt_1 + b^2t_1 + 4F_1x \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)


#assume(var("d") < 0)

#right = integrate(str2sym(r"\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou")+str2sym(r"c\exp\lou x-bt_1  \rou(\erf(x)-1)"), var("x"), 0, infinity)


#right = integrate(str2sym(r"\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou\\&+c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)

#right = integrate(str2sym(r"\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou+c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)





#right = integrate(str2sym(r"e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou"), var("x"), 0, infinity)
#right = integrate(str2sym(r"e^{-\beta V_0}c\exp\lou dx-2F_1bt_1 + b^2t_1 \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)



#right = integrate(str2sym(r"e^{-\beta V_0}\lou 1 + s_l e^{-4F_1 a}\rou\frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x+a-F_1t_1)^2}{t_1}\rou\\&+e^{-\beta V_0}c\exp\lou -2bx-2F_1bt_1 + b^2t_1 + 4F_1x \rou\lou\erf\lou\frac{a + x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), 0, infinity)


#print(integrate(str2sym(r"\exp\lou 2bx - 2F_1 bt_1 + b^2 t_1\rou(\erf(-x/f+d)-1)"), var("x"), -infinity, var("u")))
#print(integrate(str2sym(r"\exp\lou 2bx - 2F_1 bt_1 + b^2 t_1\rou\lou\erf\lou\frac{a - x + F_1 t_1 -bt_1 }{\sqrt {t_1}}\rou-1\rou"), var("x"), -infinity, 0))

#