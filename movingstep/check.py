#!/usr/bin/env python3
import sys, os
sys.path.append("/home/volker/git/latex2sympy")
import latex2sympy


texin = r"\int_{|x_0|}^\infty dz \frac 1{\sqrt{\pi t_1}}\exp\lou-\frac{(x-z-F_1t_1)^2}{t_1}+d_2z\rou d_1"

texin = texin.replace(r"\lou", r"\left(").replace(r"\rou", r"\right)")
sym = latex2sympy.latex2sympy(texin, latex2sympy.ConfigL2S())
print(sym)
