import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

dat = np.loadtxt("out/200.out")
#dat = dat[:551]
dat = dat[552:]
x = np.arange(0, len(dat))
y = dat

def func(x, a, b, c):
	return a*np.exp(-(x-b)**2/c)
popt, pcov = curve_fit(func, x,y, p0=[0.001,0,10000])
print(popt)
#popt = [0.001,300,10000]

plt.plot(x,y)
plt.plot(x, func(x, *popt))
plt.show()