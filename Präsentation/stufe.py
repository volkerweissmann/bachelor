#!/usr/bin/python3
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)

fig = plt.figure(constrained_layout=True, figsize=(3,2.5))

plt.plot([0,1,1,2,2,3],[0,1,3,4,0.5,1.5],color="blue")

plt.title("Potential")
plt.xlabel("x")
plt.ylabel("V(x)")
plt.savefig("stufe.pdf")
plt.show()